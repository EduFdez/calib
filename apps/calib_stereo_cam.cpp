/*
 *  Copyright (c) 2018 Author: Eduardo Fernandez-Moral
 *
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *      * Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *      * Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *      * Neither the name of the holder(s) nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <calib_stereo.h>
#include <iostream>

using namespace calib;
using namespace std;
using namespace cv;

#include <boost/program_options.hpp>
namespace po = boost::program_options;

int main (int argc, char ** argv)
{
    try
    {
        // Parse arguments
        const char help_msg[] = "This program computes the extrinsic calibration of a stereo camera and the rectification parameters. "
                                "It reads a pair of sequences of images that observe a checkerboard (synchronized images must have the same name (e.g. timestamp)). ";
        array<string, 2> img_dir;
        array<string, 2> file_instrinsics;
        string out_calib_file;
        int pyr(0);                        // -1 ==> automatic (based on input image size)
        int board_cols(0), board_rows(0);    // Checkerboard's dimensions
        float square_size(0);                   // Checkerboard's dimensions
        po::options_description opt("Options");
        opt.add_options()
            ("help,h", help_msg)
            ("img_dir_left,L", po::value<string>(&img_dir[0])->required(), "Directory containing the checkerboard images (left)")
            ("img_dir_right,R", po::value<string>(&img_dir[1])->required(), "Directory containing the checkerboard images (right)")
            ("instrinsics_left,l", po::value<string>(&file_instrinsics[0]), "Camera calibration file (left)")
            ("instrinsics_right,m", po::value<string>(&file_instrinsics[1]), "Camera calibration file (right)")
            ("board_cols,c", po::value<int>(&board_cols)->required(),   "Checkerboard width: number of interior columns")
            ("board_rows,r", po::value<int>(&board_rows)->required(), "Checkerboard height: number of interior rows")
            ("square_size,s", po::value<float>(&square_size)->required(), "Checkerboard square's width (in meters)")
            ("pyr,p", po::value<int>(&pyr), "optional parameter to specify the number of pyramidal levels to use")
            ("sharpen,x", "optional parameter to sharpen the images to improve corner extraction")
            ("display,d", "optional parameter to visualize the error images at each iteration (e.g. 0 -> no visualization)")
            ("calib,o", po::value<string>(&out_calib_file)->required(), "Output calibration file")
            ;
        po::variables_map vm;

        try
        {
            po::store(po::parse_command_line(argc, argv, opt), vm);
            if (vm.count("help")) {
                cout << "Usage: " << argv[0] << " [options]\n";
                cout << opt;
                return 0;
            }
            po::notify(vm);
        }
        catch(po::error& e)
        {
            cerr << "ERROR: " << e.what() << endl << endl;
            cerr << opt << endl;
            return 1; // ERROR_IN_COMMAND_LINE
        }

        StereoCalibrator calib(board_cols, board_rows, square_size);
        if( !file_instrinsics[0].empty() && !file_instrinsics[1].empty() )
            calib.load(file_instrinsics);
        calib.calibrate(img_dir, pyr, vm.count("display"));
        calib.save(out_calib_file);

//        // Uncomment to visually verify the calibration
//        if(vm.count("display"))
//            showRectified(img_dir);

        return (0);
    }
    catch (exception &e)
    {
        cerr << "Unhandled Exception reached the top of main: " << e.what() << "\n\t Exit now!. \n\n";
        return -1; // ERROR_UNHANDLED_EXCEPTION
    }
    catch (...)
    {
        printf("Unspecified exception!!");
        return -1;
    }
}
