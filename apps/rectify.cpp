/*
 *  Copyright (c) 2018 Author: Eduardo Fernandez-Moral
 *
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *      * Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *      * Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *      * Neither the name of the holder(s) nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <calib_intrinsic.h>
#include <iostream>
#include <opencv2/highgui.hpp>
#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>

using namespace calib;
using namespace std;
using namespace cv;
using namespace boost::filesystem;
namespace po = boost::program_options;

int main (int argc, char ** argv)
{
    try
    {
        // Parse arguments
        const char help_msg[] = "This program computes the intrinsic calibration of a camera (pinhole model + distortion) "
                                "from a sequence of images that observe a checkerboard. ";
        string img_dir, calib_file, out_dir;
        po::options_description opt("Options");
        opt.add_options()
            ("help,h", help_msg)
            ("img_dir,i", po::value<string>(&img_dir)->required(), "Directory containing the checkerboard images")
            ("calib,c", po::value<string>(&calib_file)->required(), "Calibration file")
//            ("fisheye,f", "optional parameter to use a fisheye camera model")
            ("display,d", "optional parameter to visualize the error images at each iteration (e.g. 0 -> no visualization)")
//            ("out_dir,o", po::value<string>(&out_dir), "Output directory")
            ;
        po::variables_map vm;

        try
        {
            po::store(po::parse_command_line(argc, argv, opt), vm);
            if (vm.count("help")) {
                cout << "Usage: " << argv[0] << " [options]\n";
                cout << opt;
                return 0;
            }
            po::notify(vm);
        }
        catch(po::error& e)
        {
            cerr << "ERROR: " << e.what() << endl << endl;
            cerr << opt << endl;
            return 1; // ERROR_IN_COMMAND_LINE
        }

        CameraCalib calib(calib_file);
        vector<path> v_path;
        copy(directory_iterator(img_dir), directory_iterator(), back_inserter(v_path));
        sort(v_path.begin(), v_path.end());
        int n(0);
        for (const path & pa : v_path) // access by const reference
        {
            //cout << "extension " << pa.extension().string() << endl;
            if( pa.extension().string() != string(".jpg") && pa.extension().string() != string(".png") )
                continue;
            if (n++ % 10 != 0)
                continue;

            cout << "img_file " << pa.string() << endl;
            cv::Mat img_rect, img_raw = imread(pa.string(), cv::IMREAD_COLOR);
            calib.rectify(img_raw, img_rect);
            cv::imshow("Raw", img_raw);
            cv::imshow("Rectified", img_rect);
            cv::waitKey();
            cv::destroyAllWindows();
        }

        return (0);
    }
    catch (exception &e)
    {
        cerr << "Unhandled Exception reached the top of main: " << e.what() << "\n\t Exit now!. \n\n";
        return -1; // ERROR_UNHANDLED_EXCEPTION
    }
    catch (...)
    {
        printf("Unspecified exception!!");
        return -1;
    }
}
