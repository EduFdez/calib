/*
 *  Copyright (c) 2019 Author: Eduardo Fernandez-Moral
 *
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *      * Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *      * Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *      * Neither the name of the holder(s) nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <calib_stereo.h>
#include <opencv2/opencv.hpp>
#include <elas/elas.h>
#include <iostream>
#include <limits>
using namespace calib;
using namespace std;
//using namespace cv;

#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
namespace po = boost::program_options;
namespace fs = boost::filesystem;

int main (int argc, char ** argv)
{
    try
    {
        // Parse arguments
        const char help_msg[] = "This program generates disparity and depth images from a sequence of left/right images. ";
        array<string, 2> img_dir;
        int max_ts_offset = 0;  // In microseconds
        string stereo_calib;
        string output_results;
        po::options_description opt("Options");
        opt.add_options()
                ("help,h", help_msg)
                ("img_dir_left,l", po::value<string>(&img_dir[0])->required(), "Directory containing the left camera images")
                ("img_dir_right,r", po::value<string>(&img_dir[1])->required(), "Directory containing the right camera images")
                ("max_ts_offset,m", po::value<int>(&max_ts_offset), "Maximum timstamp offset among left/right images")
                ("stereo_calib,C", po::value<string>(&stereo_calib)->required(), "Stereo calibration file")
                ("output_results,o", po::value<string>(&output_results)->required(), "the directory where results are saved (v_trajectories, images, etc.)")
                ("save_right_rect,R", "optional parameter to save the right cam rectified images")
                ("save_disparity,D", "optional parameter to save the left disparity images")
                ("display,d", "optional parameter to visualize the error images at each iteration (e.g. 0 -> no visualization)")
                ;
        po::variables_map vm;

        try
        {
            po::store(po::parse_command_line(argc, argv, opt), vm);
            if (vm.count("help")) {
                cout << "Usage: " << argv[0] << " [options]\n";
                cout << opt;
                return 0;
            }
            po::notify(vm);
        }
        catch(po::error& e)
        {
            cerr << "ERROR: " << e.what() << endl << endl;
            cerr << opt << endl;
            return 1; // ERROR_IN_COMMAND_LINE
        }

//        cout << "Load stereo calibration to create depth images \n";
        StereoCalibrator calib(stereo_calib);
        CalibStereoParameters stereo_params(calib.getStereoCalibParameters());
//        CalibStereoParameters stereo_params(stereo_calib);

        cout << "Processing images \n";
        array<vector<cv::String>, 2> imgs_stereo;
        for (int i = 0; i < 2; ++i)
        {
            glob((img_dir[i] + string("*.jpg")).c_str(), imgs_stereo[i], false);
//            for (auto f = imgs_stereo[i].begin(); f != imgs_stereo[i].end(); ++f)
//                cout << *f << '\n';
        }

        int left = 0;
        int right = 1;
        array<int64, 2> index{0, 0};
        array<cv::Mat,2> rgb;
        array<cv::Mat,2> crop;
        array<cv::Mat,2> img_rect;
        array<cv::Mat,2> gray_rect;
        array<int64_t, 2> timestamp;

//        // Crop images
        float bottom_crop_factor = 400 / 512.;
        cout << "size " << stereo_params.getImageSize(0).width << endl;
//        int new_height = 400
//        int new_width = 612

        float resize_factor = 1.f;
        boost::filesystem::create_directory(output_results);
        fs::path out_dir(output_results);
        if(vm.count("save_right_rect"))
            boost::filesystem::create_directory((out_dir / "right" ).string());
        if(vm.count("save_disparity"))
            boost::filesystem::create_directory((out_dir / "disparity" ).string());

        for (auto f = imgs_stereo[left].begin(); f != imgs_stereo[left].end(); ++f)
        {
            fs::path p(*f);
//            cout << "filename only          : " << p.stem() << " " << p.stem().c_str() << endl;
            {
                stringstream ss(p.stem().c_str());
                ss >> timestamp[left];
            }
            if(max_ts_offset != 0 && timestamp[left] < 1)
                continue;

            // Get pair
            {
                fs::path p_right(imgs_stereo[right][index[right]]);
                stringstream ss(p_right.stem().c_str());
                ss >> timestamp[right];
            }
            cout << distance(imgs_stereo[left].begin(), f) << " timestamps " << timestamp[left] << " " << timestamp[right] << " diff " << timestamp[left] - timestamp[right] << endl;

//            while(timestamp[left] > timestamp[right])  // Increase the right counter
            while(timestamp[left] > timestamp[right] + max_ts_offset)  // Increase the right counter
            {
                fs::path p_right(imgs_stereo[right][++index[right]]);
                stringstream ss(p_right.stem().c_str());
                ss >> timestamp[right];
            }

//            if(timestamp[left] == timestamp[right])
            if(abs(timestamp[left] - timestamp[right]) <= max_ts_offset)  // timstamp in microseconds
            {
                cout << "Process " << *f << endl;
                cout << "Timestamp diff " << (timestamp[left] - timestamp[right]) << endl;
//                cout << "pair \n\t" << *f << "\n\t" << imgs_stereo[right][index[right]] << endl;

                // Generate disparity
                rgb[left] = cv::imread(*f, cv::IMREAD_COLOR); //CV_LOAD_IMAGE_COLOR);
                rgb[right] = cv::imread(imgs_stereo[right][index[right]], cv::IMREAD_COLOR); //CV_LOAD_IMAGE_COLOR);

//                // Crop Marseille dataset images
//                cv::Rect roi = cv::Rect(0, 0, new_width, new_height); // Select only a portion of the image with height = width/4 (90 deg) with the FOV centered at the equator. This increases the performance of dense registration at the cost of losing some details from the upper/lower part of the images, which generally capture the sky and the floor.
//                crop[left] = rgb[left](roi);
//                crop[left].copyTo(rgb[left]);
//                cv::imwrite((out_dir / p.filename()).string() , rgb[left]);
//
//                crop[right] = rgb[right](roi);
//                crop[right].copyTo(rgb[right]);
////                cv::Mat depth_crop = depth_scaled(roi);
////                cv::imwrite((out_dir / file_name).string() , depth_scaled);

//                flip(rgb[left], rgb[left], 1);
//                flip(rgb[right], rgb[right], 1);

                calib.rectifyPair(rgb, img_rect);

                cv::cvtColor(img_rect[left], gray_rect[left], cv::COLOR_BGR2GRAY); //CV_BGR2GRAY);
                cv::cvtColor(img_rect[right], gray_rect[right], cv::COLOR_BGR2GRAY); //CV_BGR2GRAY);

                const cv::Size imsize = gray_rect[0].size();
                if(resize_factor != 1.f)
                {
                    cout << "Resize input images" << endl;
                    cv::resize(gray_rect[left], gray_rect[left], cv::Size(), 1/resize_factor, 1/resize_factor);
                    cv::resize(gray_rect[right], gray_rect[right], cv::Size(), 1/resize_factor, 1/resize_factor);
                }

                ++index[right];

                const cv::Size imsize_disp = gray_rect[0].size();
//                cout << "imsize " << imsize_disp << " step " << gray_rect[0].step[0] << " step " << gray_rect[0].step[1] << endl;
                const int32_t dims[3] = {imsize_disp.width, imsize_disp.height, int(gray_rect[0].step[0])};
                array<cv::Mat,2> disp = {cv::Mat::zeros(imsize_disp, CV_32F), cv::Mat::zeros(imsize_disp, CV_32F)};

                Elas::parameters param(Elas::ROBOTICS);
                param.postprocess_only_left = true;
                Elas elas(param);
                elas.process(gray_rect[0].data, gray_rect[1].data, disp[0].ptr<float>(0), disp[1].ptr<float>(0), dims);
//                for(unsigned i=0; i < disp[0].total(); i++)
//                    cout << disp[0].at<float>(i) << " ";

                if(resize_factor != 1.f)
                {
                    disp[0] = disp[0] / resize_factor;
                    cv::resize(disp[0], disp[0], imsize, 0, 0, cv::INTER_NEAREST);
//                    cv::resize(disp[1], disp[1], imsize, 0, 0);
                }

                // Scale depth iamges before saving
//                cout << "disparity = "<< endl << " "  << disp[0] << endl << endl;
                float focal_baseline_factor = stereo_params.getfocalLengthHorizontal() * stereo_params.getBaseLine(); // * 16.0; // only one focal length, calculations are not precise, disparity is scaled by 16
//                Mat depth;
//                divide(focal_baseline_factor, disp[0], depth, double scale=1000);
                const float depth_factor = 500.f;  //500.f;   1000.f;
                cv::Mat depth = (depth_factor * focal_baseline_factor) / disp[0]; // Scaled by depth_factor

                double min, max;
                cv::minMaxLoc(depth, &min, &max);
                cout << "depth " <<depth.type() << " " << min << " " << max << endl;

                cv::Mat depth_scaled;
                depth.convertTo(depth_scaled, CV_16UC1); // In millimeters

                if(vm.count("display")) {
//                    double min, max;
//                    cv::minMaxLoc(disp[0], &min, &max);
//                    cout << " min " << min << " max " << max << endl;
//                    cv::Mat min_disp_mask = disp[0] == min;
//                    cv::Mat max_disp_mask = disp[0] == max;
//                    cv::imshow("min_disp_mask", min_disp_mask);
//                    cv::imshow("max_disp_mask", max_disp_mask);
                    cv::imshow("Disparity left", disp[left]);
                    cv::imshow("Disparity right", disp[right]);
                    calib.showRectified(img_rect);
                    cv::waitKey(0);
                }

                fs::path file_name(p.stem().string() + ".png");
                cout << "Output: " << (out_dir / file_name).string() << endl;
//                cv::imwrite((out_dir / file_name).string() , depth_scaled);

                cv::imwrite((out_dir / file_name).string() , depth_scaled);
                cv::imwrite((out_dir / p.filename()).string() , img_rect[left]);
                if(vm.count("save_right_rect"))
                    cv::imwrite((out_dir / "right" / p.filename()).string() , img_rect[right]);

                if(vm.count("save_disparity"))
                {
                    cv::Mat invalid_disparity = disp[left] == -10;
//                    cv::imshow("invalid_disparity", invalid_disparity);
//                    cv::waitKey(0);
                    double min, max;
                    cv::minMaxLoc(disp[left], &min, &max);
                    const float disp_factor = (numeric_limits<uint16_t>::max() - 1) / param.disp_max;// 255.f;  //500.f;   1000.f;
                    disp[left] *= disp_factor;  // Re-scale disparity
                    cv::Mat disp_scaled;
                    disp[left].convertTo(disp_scaled, CV_16UC1); // In millimeters
                    disp_scaled.setTo(cv::Scalar(numeric_limits<uint16_t>::max()), invalid_disparity);
                    cv::imwrite((out_dir / "disparity" / file_name).string() , disp_scaled);
                }

//                // Crop images to remove the ego-vehicle
//                cv::Rect roi = cv::Rect(0, 0, depth.cols, int(depth.rows*bottom_crop_factor));
//                cv::imwrite((out_dir / file_name).string() , depth_scaled(roi));
//                cv::imwrite((out_dir / p.filename()).string() , img_rect[left](roi));
            }
            else
                continue;
        }

        return (0);
    }
    catch (exception &e)
    {
        cerr << "Unhandled Exception reached the top of main: " << e.what() << "\n\t program will now exit. \n\n";
        return -1; // ERROR_UNHANDLED_EXCEPTION
    }
    catch (...)
    {
        printf("Unspecified exception!!");
        return -1;
    }
}
