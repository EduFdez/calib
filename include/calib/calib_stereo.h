/*
 *  Copyright (c) 2018 Author: Eduardo Fernandez-Moral
 *
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *      * Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *      * Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *      * Neither the name of the holder(s) nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#pragma once

#include "calib_intrinsic.h"

namespace calib
{
    /*! Stereo parameters.
     *  \ingroup calib_group
     */
    class CalibStereoParameters
    {
    public:
        /*! Intrinsic parameters */
        std::array<CameraCalib, 2> cam;

        /*! Fundamental Matrix */
        cv::Mat F;

        /*! Essential Matrix */
        cv::Mat E;

        /*! Rotation among the 2 cameras */
        cv::Mat Rot;

        /*! Translation among the 2 cameras */
        cv::Vec3d T;

        /*! Rectification parameters */
        cv::Mat Q; //, R1, R2, P1, P2;

        CalibStereoParameters();

        CalibStereoParameters(const std::string & file_name);

        void load(const std::string& file_name);
        void save(const std::string& out_file);

        const cv::Size getImageSize(int camNo = 0) const {
            return cam[camNo].img_size;
        }

        const double getfocalLengthHorizontal(int camNo = 0) const {
            return cam[camNo].K.at<double>(0, 0);
        }

        const double getfocalLengthVertical(int camNo = 0) const {
            return cam[camNo].K.at<double>(1, 1);
        }

        const double getCenterHorizontal(int camNo = 0) const {
            return cam[camNo].K.at<double>(0, 2);
        }

        const double getCenterVertical(int camNo = 0) const {
            return cam[camNo].K.at<double>(1, 2);
        }

        const double getOpeningAngleHorizontal(int camNo = 0) const {
            return 2.0 * atan(cam[camNo].img_size.width / (2.0 * cam[camNo].K.at<double>(0, 0)));
        }

        const double getOpeningAngleVertical(int camNo = 0) const {
            return 2.0 * atan(cam[camNo].img_size.height / (2.0 * cam[camNo].K.at<double>(1, 1)));
        }

        const double getBaseLine() const {
            return -T[0];
        }

    };

    /*! This class computes the extrinsic calibration of a stereo camera from the observartions of a checkerboard.
     *
     *  \ingroup calib_group
     */
    class StereoCalibrator : protected CameraCalibrator
    {
      public:

        /*! Constructor */
        StereoCalibrator(const std::string & calib_file);

        /*! Constructor */
        StereoCalibrator(int board_cols_, int board_rows_, float square_size_);

        /*! Destructor */
        virtual ~StereoCalibrator() {}

        /*! Load the intrinsic parameters of each one of the 2 cameras */
        inline void load(const std::array<std::string, 2> & file_instrinsics)
        {
            compute_intrinsics = false;
            stereo_calib.cam[0].setIntrinsics(IntrinsicsPinhole::load(file_instrinsics[0]));
            stereo_calib.cam[1].setIntrinsics(IntrinsicsPinhole::load(file_instrinsics[1]));
        }

        /*! Find the checkerboard corners in the images in 'imgs_directory' */
        void findCorners(const std::array<std::string, 2> &img_dir, int pyr = 0, bool display = false);

        /*! Compute the calibration */
        void calibrate(const std::array<std::string, 2> & img_dir, int pyr = 0, bool display = false);

        /*! Display the rectified calibration images with corresponding Epipolar lines */
        void showRectified(const std::array<cv::Mat,2> & img_rect);

        /*! Display the rectified calibration images with corresponding Epipolar lines */
        void showRectified(const std::array<std::string, 2> & img_dir);

        /*! Save the calibration */
        void save(const std::string & out_file);

        const CalibStereoParameters & getStereoCalibParameters() const;

        /*! Load the calibration */
        static CalibStereoParameters load(const std::string & calib_file);

        /*! Pre-calculate the rectification maps */
        inline void calcRectificationMaps() {
            if (stereo_calib.cam[0].K.empty() || stereo_calib.cam[1].K.empty())
              throw std::runtime_error("Stereo calcRectificationMaps: K is empty");
            cv::initUndistortRectifyMap(stereo_calib.cam[0].K, stereo_calib.cam[0].D, stereo_calib.cam[0].R, stereo_calib.cam[0].P, stereo_calib.cam[0].img_size, CV_32F,
                    stereo_calib.cam[0].mapXY[0], stereo_calib.cam[0].mapXY[1]);
            cv::initUndistortRectifyMap(stereo_calib.cam[1].K, stereo_calib.cam[1].D, stereo_calib.cam[1].R, stereo_calib.cam[1].P, stereo_calib.cam[1].img_size, CV_32F,
                    stereo_calib.cam[1].mapXY[0], stereo_calib.cam[1].mapXY[1]);
        }

        /*! Rectify the stereo image pair */
        inline void rectifyPair(const std::array<cv::Mat,2> & raw, std::array<cv::Mat,2> & undistorted)
        {
            if (stereo_calib.cam[0].mapXY[0].empty() || stereo_calib.cam[0].mapXY[0].empty())
              throw std::runtime_error("rectify: RectificationMaps empty");
            // TODO: use gpu::remap for better performance
            cv::remap(raw[0], undistorted[0], stereo_calib.cam[0].mapXY[0], stereo_calib.cam[0].mapXY[1], cv::INTER_LINEAR);
            cv::remap(raw[1], undistorted[1], stereo_calib.cam[1].mapXY[0], stereo_calib.cam[1].mapXY[1], cv::INTER_LINEAR);
        }

      protected:
        /*! Intrinsic parameters */
        CalibStereoParameters stereo_calib;
        /*! Raw stereo image pair */
        std::array<cv::Mat,2> img;
        /*! Raw stereo grayscale image pair */
        std::array<cv::Mat,2> gray;
        /*! Rectified stereo image pair */
        std::array<cv::Mat,2> img_rect;
        /*! Rectified stereo grayscale image pair */
        std::array<cv::Mat,2> gray_rect;

      private:
        bool compute_intrinsics;
        std::array<cv::Mat,2> gray_sharp;
        std::array< std::vector< cv::Point2f >, 2> corners;
        std::array< std::vector< std::vector< cv::Point2f > >, 2> img_points;
    };

}
