/*
 *  Copyright (c) 2018 Author: Eduardo Fernandez-Moral
 *
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *      * Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *      * Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *      * Neither the name of the holder(s) nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#pragma once

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/calib3d.hpp>
#include <array>
#include <deque>
#include <iostream>

namespace calib
{
  /*! Store the intrinsic parameters of a camera (pinhole). It contains the projection matrix and the distortion params.
   *  \ingroup calib_group
   */
  struct IntrinsicsPinhole
  {
    /*! Image size */
    cv::Size img_size;

    /*! Camera matrix (pinhole model projection) */
    cv::Mat K;

    /*! Distortion parameters */
    cv::Mat D;

    /*! Has fisheye distortion parameters */
    bool fisheye;

//        inline bool & getFisheye() { return fisheye;}

    IntrinsicsPinhole(bool fisheye_ = false) : fisheye(fisheye_) {}

    explicit IntrinsicsPinhole(const std::string & calib_file);

    /*! Save the calibration
     *  \param calib_file
     */
    void save(const std::string & calib_file, float rms = 0.f);

    /*! Load the calibration
      *  \param calib_file
      */
    static IntrinsicsPinhole load(const std::string & calib_file);
  };

  /*! Store the the rectification parameters of a camera (pinhole).
   *  \ingroup calib_group
   */
  struct CameraCalib : public IntrinsicsPinhole
  {
    /*! Rectification maps (undistortion) */
    std::array<cv::Mat, 2> mapXY;

    /*! New camera matrix */
    cv::Mat P;

    /*! Rotation matrix */
    cv::Mat R;

    /*! Pre-calculate the rectification maps */
    inline void calcRectificationMaps(float alpha = 0.5) {
      if (K.empty())
        throw std::runtime_error("calcRectificationMaps: K is empty");
//          if(R.empty())
//            R = cv::Mat::eye(3,3,CV_64F);
      if (fisheye) {
        cv::fisheye::estimateNewCameraMatrixForUndistortRectify(K, D, img_size, R, P, alpha);
        cv::fisheye::initUndistortRectifyMap(K, D, R, P, img_size, CV_32F, mapXY[0], mapXY[1]);
      } else {
        P = cv::getOptimalNewCameraMatrix(K, D, img_size, alpha);
        cv::initUndistortRectifyMap(K, D, R, P, img_size, CV_32F, mapXY[0], mapXY[1]);
//            cv::initWideAngleProjMap(K, D, img_size, img_size.width, CV_32F, mapXY[0], mapXY[1], cv::PROJ_SPHERICAL_EQRECT, 0.5);
      }
//          print();
    }

    /*! Rectify the image
      *  \param raw image
      *  \param rect rectified image
      */
    inline void rectify(const cv::Mat & raw, cv::Mat & rect) {
      if (mapXY[0].empty() || mapXY[1].empty())
        throw std::runtime_error("rectify: RectificationMaps empty");
      cv::remap(raw, rect, mapXY[0], mapXY[1], cv::INTER_LINEAR);
    }

    void print() {
      std::cout << "\n CameraCalib fisheye " << fisheye << std::endl;
      std::cout << "K \n" << K << std::endl;
      std::cout << "D \n" << D << std::endl;
      std::cout << "P \n" << P << std::endl;
      std::cout << "R \n" << R << std::endl;
    }

    CameraCalib(bool fisheye = false) : IntrinsicsPinhole(fisheye) {}

    explicit CameraCalib(const std::string & calib_file) : IntrinsicsPinhole(calib_file) {
      calcRectificationMaps();
    }

    explicit CameraCalib(const IntrinsicsPinhole & intrinsic_params) : IntrinsicsPinhole(intrinsic_params) {
      calcRectificationMaps();
    }

    void setIntrinsics(const IntrinsicsPinhole & intrinsic_params) {
      *dynamic_cast<IntrinsicsPinhole*>(this) = intrinsic_params;
    }
  };

  /*! This class computes the intrinsic calibration of a camera from the observartions of a checkerboard.
   *
   *  \ingroup calib_group
   */
  class CameraCalibrator : public CameraCalib
  {
  public:

    /*! Constructor
     *  \param calib_file
     */
    explicit CameraCalibrator(const std::string & calib_file);

    /*! Constructor
     *  \param board_cols_ internal columns of the checkerboard (number of columns - 1)
     *  \param board_rows_ internal rows of the checkerboard (number of columns - 1)
     *  \param square_size_ square side length in meters
     */
    CameraCalibrator(int board_cols_ = 0, int board_rows_ = 0, float square_size_ = 0, bool fisheye_ = false);

    /*! Destructor */
    virtual ~CameraCalibrator() {}

    int findCorners(const cv::Mat & img, int pyr = 0, bool display = false);

    /*! Find the checkerboard corners in the images in 'imgs_directory'
     *  \param imgs_directory
     *  \param pyr is the level pyramid where the coarse search for corners starts (0 represents the original resolution)
     *  \param display
     */
    void findCorners(const std::string & imgs_directory, int pyr = 0, bool display = false);

    /*! Compute the reprojection error
     *  \param objectPoints the 3D points of the detected pattern (the Z coordinate is set to 0 for the flat checkerboard)
     *  \param imagePoints the image coordinates of the detected pattern
     *  \param rvec rotation vectors (one per detection)
     *  \param tvec translation vectors (one per detection)
     *  \param cameraMatrix
     *  \param distCoeffs
     */
    static double computeReprojectionErrors(const std::vector< std::vector< cv::Point3f > >& objectPoints,
                                            const std::vector< std::vector< cv::Point2f > >& imagePoints,
                                            const std::vector< cv::Mat >& rvec, const std::vector< cv::Mat >& tvec,
                                            const cv::Mat& cameraMatrix , const cv::Mat& distCoeffs,
                                            std::vector<float>& perViewErrors, bool fisheye = false);

    /*! Compute the calibration
     *  \param calib_file
     */
    double calibrate(const std::string & imgs_directory, int pyr = 0, bool display = false);

    double computeCalibration(size_t max_obs = 0, float max_rms_per_view = 0.f, int flag = 0);

  protected:
    int board_cols, board_rows;
    float square_size;
    std::vector< cv::Point3f > checkerboard;

    /*! Set the real size of the checkerboard pattern */
    void setCheckerboardSize();

    inline size_t getNumDetections() {return image_points.size();}

  private:
    /*! Temporary containers for pattern observations */
    cv::Mat img, gray, gray_sharp;
    std::deque< std::vector< cv::Point2f > > image_points;
    size_t img_pts_max_size = 500;
    std::vector< cv::Point2f > corners;
  };
}
