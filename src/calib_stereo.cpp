/*
 *  Copyright (c) 2018 Author: Eduardo Fernandez-Moral
 *
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *      * Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *      * Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *      * Neither the name of the holder(s) nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "calib_stereo.h"
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <boost/filesystem.hpp>
#include <algorithm>
#include <numeric>
#include <iostream>
#include <chrono>

using namespace calib;
using namespace std;
using namespace cv;
using namespace chrono;
using namespace boost::filesystem;

CalibStereoParameters::CalibStereoParameters() {}

CalibStereoParameters::CalibStereoParameters(const std::string& file_name)
{
  load(file_name);
}

void CalibStereoParameters::save(const string & out_file)
{
  cout << "StereoCalibrator::saveStereoCalib ... " << out_file << endl;
  FileStorage fs(out_file, FileStorage::WRITE);
//    fs << "size" << cam[0].img_size; //.width << img_size.height;
  Mat size(2, 1, CV_32SC1, (void*)&cam[0].img_size);
  fs << "size" << size; //.width << img_size.height;
  fs << "K1" << cam[0].K;
  fs << "K2" << cam[1].K;
  fs << "D1" << cam[0].D;
  fs << "D2" << cam[1].D;
  fs << "R" << Rot;
  fs << "T" << T;
  fs << "E" << E;
  fs << "F" << F;
  // Rectification
  fs << "R1" << cam[0].R;
  fs << "R2" << cam[1].R;
  fs << "P1" << cam[0].P;
  fs << "P2" << cam[1].P;
  fs << "Q" << Q;
  fs.release();
}

void CalibStereoParameters::load(const string & calib_file)
{
  if (!exists(calib_file))
    throw std::runtime_error("Calibration file does not exists: " + calib_file);

  FileStorage fs(calib_file, FileStorage::READ | FileStorage::FORMAT_YAML);
  auto sizeNode = fs["size"];
  cv::Mat img_size;
  sizeNode >> img_size;
  //sizeNode >> stereo_params.cam[0].img_size;
  //fs["size"] >> stereo_params.cam[1].img_size;
  cam[0].img_size.width = cam[1].img_size.width = img_size.at<int>(0, 0);
  cam[0].img_size.height = cam[1].img_size.height = img_size.at<int>(1, 0);
  fs["K1"] >> cam[0].K;
  fs["K2"] >> cam[1].K;
  fs["D1"] >> cam[0].D;
  fs["D2"] >> cam[1].D;
  fs["R"] >> Rot;
  cv::Mat t;
  fs["T"] >> t;
  T = t;
  // Rectification
  fs["R1"] >> cam[0].R;
  fs["R2"] >> cam[1].R;
  fs["P1"] >> cam[0].P;
  fs["P2"] >> cam[1].P;
  fs["Q"] >> Q;
}


// ====================================================================================================
//                                     StereoCalibrator
// ====================================================================================================


StereoCalibrator::StereoCalibrator(const string & calib_file)
                    :stereo_calib(calib_file)
{
    calcRectificationMaps();
}

StereoCalibrator::StereoCalibrator(int board_cols_, int board_rows_, float square_size_) :
    CameraCalibrator(board_cols_, board_rows_, square_size_),
    compute_intrinsics(true)
{}

void StereoCalibrator::findCorners(const array<string, 2> & img_dir, int pyr, bool display)
{
    assert(board_cols > 0 && board_rows > 0 && square_size > 0.f); // Check that the physical information of a checjerboard is present
    assert(pyr >= 0);

    if(display)
    {
        cv::namedWindow("corners0", cv::WINDOW_AUTOSIZE);
        cv::namedWindow("corners1", cv::WINDOW_AUTOSIZE);
    }
    array<vector<int>, 2> detections_per_pyr{vector<int>(pyr+1, 0), vector<int>(pyr+1, 0)};
    int n_pairs(0);
    int n_detec(0);

    // Measuere exucution time
    steady_clock::time_point t1 = steady_clock::now();

    Size board_size = Size(board_cols, board_rows);
    vector<path> v_path_left;
    copy(directory_iterator(img_dir[0]), directory_iterator(), back_inserter(v_path_left));
    sort(v_path_left.begin(), v_path_left.end());
    for(int i=0; i<2; i++)
        img_points[i].reserve( v_path_left.size() );
    for (const path & pa : v_path_left) // access by const reference
    {
        array<bool, 2> detected{false, false};
        array<string, 2> img_path{pa.string(), ""};
        img_path[1] = string(img_dir[1]) + "/" + pa.filename().string(); // pa.parent_path().string() + "/" + pa.filename().string();

        // Check that both images (left and right) exist
        if( !boost::filesystem::exists(img_path[0]) || !boost::filesystem::exists(img_path[1]) )
            continue;

        ++n_pairs;

        //cout << "left_img " << img_path[0] << endl << "right_img " << img_path[1] << endl;
        array<int, 2> py{pyr, pyr};
        for(int i=0; i<2; i++)
        {
            img[i] = imread(img_path[i], cv::IMREAD_COLOR);
            if(img[i].channels() > 1)
                cvtColor(img[i], gray[i], cv::COLOR_BGR2GRAY);

            // Downsample the input image to improve corner detection (fixed size kernel detector in OpenCV)
            vector<Mat> gray_pyr(pyr+1);
            gray_pyr[0] = gray[i];
            for(int p=0 ; p < pyr; p++)
            {
                gray_pyr[p+1] = cv::Mat(gray_pyr[p].rows/2, gray_pyr[p].cols/2, gray_pyr[p].type());
                pyrDown( gray_pyr[p], gray_pyr[p+1], gray_pyr[p+1].size() ); // cv::Size( gray_low_res.cols/2, gray_low_res.rows/2 )
            }
            for( ; (py[i] >= 0 && !detected[i]); py[i]--)
                detected[i] = cv::findChessboardCorners(gray_pyr[py[i]], board_size, corners[i], CALIB_CB_ADAPTIVE_THRESH | CALIB_CB_FILTER_QUADS); // CALIB_CB_ADAPTIVE_THRESH+CALIB_CB_NORMALIZE_IMAGE);
            if( detected[i] )
            {
                if( ++py[i] > 0 ) // Set the pyramid level to the last one for which findChessboardCorners was run
                    transform(corners[i].begin(), corners[i].end(), corners[i].begin(), [py, i](Point2f & item) -> Point2f { return float(exp2(py[i]))*item; } );
                ++detections_per_pyr[i][py[i]];
            }
        }

        if(detected[0] && detected[1])
        {
            for(int i=0; i<2; i++)
            {
                cv::cornerSubPix(gray[i], corners[i], cv::Size(11, 11), cv::Size(-1, -1), TermCriteria(TermCriteria::EPS | TermCriteria::MAX_ITER, 30, 0.1));
                img_points[i].push_back(corners[i]);
            }
            ++n_detec;
            cout << img_path[0] << ". Found corners! " << py[0] << " " << py[1] << endl;
        }
        else
            cout << " No corners detected in " << img_path[0] << " " << detected[0] << " " << detected[1] << endl;

        if(display)
        {
            for(int i=0; i<2; i++)
            {
                cv::drawChessboardCorners(img[i], board_size, corners[i], detected[i]);
                imshow(string("corners")+to_string(i), img[i]);
            }
            waitKey(0);
        }
    }    
    destroyWindow("corners0");
    destroyWindow("corners1");

    if( !display )
    {
        steady_clock::time_point t2 = steady_clock::now();
        auto duration = duration_cast<milliseconds>( t2 - t1 ).count();
        cout << "StereoCalibrator::findCorners took " << duration/(float)1e3 << " seconds. \n";
    }

    // Print summary of corner detections
    cout << "\n Sumary of corners detected. \n Total pairs: " << n_detec << " / " << n_pairs << "\n";
    for(int i=0; i<2; i++)
    {
        int n_detections = accumulate(detections_per_pyr[i].begin(), detections_per_pyr[i].end(), 0);
        int missed = n_pairs-n_detections;
        vector<float> detections_rate(pyr+1);
        transform(detections_per_pyr[i].begin(), detections_per_pyr[i].end(), detections_rate.begin(), [n_pairs](int x) -> float { return x / (float)n_pairs; });
        cout << "cam" << i << " detections: " << n_detections << " missed: " << missed << " \n pyramids: ";
        copy(detections_per_pyr[i].begin(), detections_per_pyr[i].end(), ostream_iterator<int>(cout, " / "));
        cout << " ratios ";
        copy(detections_rate.begin(), detections_rate.end(), ostream_iterator<float>(cout, " / "));
        cout << " missed " << (missed/(float)n_pairs) << "\n\n";
    }

    for(int i=0; i<2; i++)
        stereo_calib.cam[i].img_size = img[i].size();
}

void StereoCalibrator::calibrate(const array<string, 2> & img_dir, int pyr, bool display)
{
    cout << "CameraCalibrator::calibrate()... " << endl;
    // Measuere exucution time
    steady_clock::time_point t1 = steady_clock::now();

    pyr = (pyr >= 0) ? pyr : 0; // pyr must be positive

    findCorners(img_dir, pyr, display);
    vector< vector< cv::Point3f > > checkerboard_points(img_points[0].size(), checkerboard);

    int flag = 0;
    if(!compute_intrinsics)
        flag |= CALIB_FIX_INTRINSIC;
    double rms = cv::stereoCalibrate(checkerboard_points, img_points[0], img_points[1], stereo_calib.cam[0].K, stereo_calib.cam[0].D, stereo_calib.cam[1].K, stereo_calib.cam[1].D,
            stereo_calib.cam[0].img_size, stereo_calib.Rot, stereo_calib.T, stereo_calib.E, stereo_calib.F, flag, TermCriteria(TermCriteria::COUNT+TermCriteria::EPS, 30, 1e-6));

    // Compute Rectification maps
    stereoRectify ( stereo_calib.cam[0].K, stereo_calib.cam[0].D, stereo_calib.cam[1].K, stereo_calib.cam[1].D, stereo_calib.cam[0].img_size, stereo_calib.Rot, stereo_calib.T,
                    stereo_calib.cam[0].R, stereo_calib.cam[1].R, stereo_calib.cam[0].P, stereo_calib.cam[1].P, stereo_calib.Q);
    if( !display )
    {
        steady_clock::time_point t2 = steady_clock::now();
        auto duration = duration_cast<milliseconds>( t2 - t1 ).count();
        cout << "StereoCalibrator::calibrate took " << duration/(float)1e3 << " seconds. \n";
    }
    cout << rms << " RMS. Rotation \n" << stereo_calib.Rot << "\n translation \n" << stereo_calib.T << endl;
}

void StereoCalibrator::showRectified(const std::array<cv::Mat,2> & img_rect)
{
    const int sep = 20;
    const int n_epi_lines = 20;
    cv::Mat display_img_rect(img_rect[0].rows, 2*img_rect[0].cols+sep, img_rect[0].type(), cv::Scalar(50,50,50));
    img_rect[0].copyTo( display_img_rect(cv::Rect(0, 0, img_rect[0].cols, img_rect[0].rows) ) );
    img_rect[1].copyTo( display_img_rect(cv::Rect(img_rect[0].cols+sep, 0, img_rect[0].cols, img_rect[0].rows) ) );
    for(int r=img_rect[0].rows/n_epi_lines; r < img_rect[0].rows; r+=img_rect[0].rows/n_epi_lines)
        cv::line(display_img_rect, Point(0,r), Point(display_img_rect.cols,r), Scalar(255,0,0),1);

    cv::imshow("display_img_rect", display_img_rect);
    cv::waitKey(0);
}

void StereoCalibrator::showRectified(const array<string, 2> & img_dir)
{
    calcRectificationMaps();
    vector<path> v_path_left;
    copy(directory_iterator(img_dir[0]), directory_iterator(), back_inserter(v_path_left));
    sort(v_path_left.begin(), v_path_left.end());
    for (const path & pa : v_path_left) // access by const reference
    {
        array<string, 2> img_path{pa.string(), ""};
        img_path[1] = string(img_dir[1]) + "/" + pa.filename().string(); // pa.parent_path().string() + "/" + pa.filename().string();
        for(int i=0; i<2; i++)
            img[i] = imread(img_path[i], cv::IMREAD_COLOR);
        rectifyPair(img, img_rect);
        showRectified(img_rect);
    }
}

void StereoCalibrator::save(const string & out_file)
{
    stereo_calib.save(out_file);
}

CalibStereoParameters StereoCalibrator::load(const string & calib_file)
{
    CalibStereoParameters stereo_params(calib_file);
    return stereo_params;
}

const CalibStereoParameters &StereoCalibrator::getStereoCalibParameters() const {
    return stereo_calib;
}
