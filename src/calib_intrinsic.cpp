/*
 *  Copyright (c) 2018 Author: Eduardo Fernandez-Moral
 *
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *      * Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *      * Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *      * Neither the name of the holder(s) nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "calib_intrinsic.h"
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <boost/filesystem.hpp>
#include <algorithm>
#include <numeric>
#include <iostream>
#include <chrono>
#include <random>

using namespace calib;
using namespace std;
using namespace cv;
using namespace std::chrono;
using namespace boost::filesystem;

IntrinsicsPinhole::IntrinsicsPinhole(const std::string & calib_file) {
  if (!exists(calib_file))
    throw std::runtime_error("Calibration file does not exists: " + calib_file);

  FileStorage fs(calib_file, FileStorage::READ);
  cv::Mat im_size;
  fs["size"] >> im_size;
  img_size = Size(im_size.at<int>(0), im_size.at<int>(1));
  fs["K"] >> K;
  fs["D"] >> D;
  fisheye = D.total() == 4;
}

IntrinsicsPinhole IntrinsicsPinhole::load(const string & calib_file)
{
  //cout << "CameraCalibrator::load ... " << endl;
  if (!exists(calib_file))
    throw std::runtime_error("Calibration file does not exists: " + calib_file);
  
  IntrinsicsPinhole cam_params;
  FileStorage fs(calib_file, FileStorage::READ);
  cv::Mat img_size;
  fs["size"] >> img_size;
  cam_params.img_size = Size(img_size.at<int>(0), img_size.at<int>(1));
//    cout << "IntrinsicsPinhole CameraCalibrator::load size " << img_size << " " << cam_params.img_size << endl;
//    fs["size"] >> cam_params.img_size; //cam_params.img_size.width >> img_size.height;
  fs["K"] >> cam_params.K;
  fs["D"] >> cam_params.D;
  cam_params.fisheye = cam_params.D.total() == 4;
  return cam_params;
}

void IntrinsicsPinhole::save(const string & calib_file, float rms)
{
  //cout << "CameraCalibrator::save ... " << endl;
  FileStorage fs(calib_file, FileStorage::WRITE);
  Mat size(2, 1, CV_32SC1, (void*)&img_size);
  fs << "size" << size; //.width << img_size.height;
//    fs << "size" << img_size; //.width << img_size.height;
  fs << "K" << K;
  fs << "D" << D;
  if (rms > 1e-6)
    fs << "rms" << rms;
  fs.release();
}

CameraCalibrator::CameraCalibrator(const string & calib_file) :
        CameraCalib(calib_file)
{
}

CameraCalibrator::CameraCalibrator(int board_cols_, int board_rows_, float square_size_, bool fisheye_) :
        CameraCalib(fisheye_),
        board_cols(board_cols_),
        board_rows(board_rows_),
        square_size(square_size_)
{
  setCheckerboardSize();
}

int CameraCalibrator::findCorners(const cv::Mat & img, int pyr, bool display) {
  auto t1 = std::chrono::steady_clock::now();

  bool detected(false);
  Size board_size = Size(board_cols, board_rows);
  img_size = img.size();
  if(img.channels() > 1)
    cv::cvtColor(img, gray, cv::COLOR_BGR2GRAY);

//        // Sharpen the image
//        bool b_sharpen= false;
//        Point anchor = Point(0, 0);
//        int delta = 0;
//        int ddepth = -1;
//        int kernel_size = 3;
//        Mat kernel = Mat::zeros( kernel_size, kernel_size, CV_32F); /// (float)(kernel_size*kernel_size);
//        kernel.at<float>(0,1) = kernel.at<float>(1,0) = kernel.at<float>(1,2) = kernel.at<float>(2,1) = -1;
//        kernel.at<float>(1,1) = 5;
//        filter2D(gray, gray_sharp, ddepth, kernel, anchor, delta, BORDER_DEFAULT );
//        //    cv::GaussianBlur(gray, gray_sharp, cv::Size(0, 0), 3);
//        imshow("gray", gray);
//        imshow("gray_sharp", gray_sharp);
//        waitKey(0);
//        destroyWindow("gray_sharp");
//        cv::addWeighted(gray, 1.5, gray_sharp, -0.5, 0, gray_sharp);
//        imshow("gray_sharp", gray_sharp);
//        waitKey(0);
//        destroyWindow("gray_sharp");
//        destroyWindow("gray");
//        gray = gray_sharp;

  vector<Mat> gray_pyr(pyr+1);
  gray_pyr[0] = gray;
  int p(0);
  for( ; p < pyr; p++)
  {
    gray_pyr[p+1] = cv::Mat(gray_pyr[p].rows/2, gray_pyr[p].cols/2, gray_pyr[p].type());
    pyrDown( gray_pyr[p], gray_pyr[p+1], gray_pyr[p+1].size() ); // cv::Size( gray_low_res.cols/2, gray_low_res.rows/2 )
  }
  int flags = CALIB_CB_ADAPTIVE_THRESH | CALIB_CB_FILTER_QUADS; // CALIB_CB_NORMALIZE_IMAGE
  if (fisheye)
    flags |= CALIB_CB_FAST_CHECK;
  for( ; (p >= 0 && !detected); p--)
    detected = cv::findChessboardCorners(gray_pyr[p], board_size, corners, flags);

  if (detected)
  {
    if( ++p > 0 ) // Set the pyramid level to the last one for which findChessboardCorners was run
      transform(corners.begin(), corners.end(), corners.begin(), [p](Point2f & item) -> Point2f { return (float)exp2(p)*item; } );
    cornerSubPix(gray, corners, cv::Size(11, 11), cv::Size(-1, -1), TermCriteria(TermCriteria::EPS | TermCriteria::MAX_ITER, 30, 0.1));
    image_points.push_back(corners);
    while(image_points.size() > img_pts_max_size)
      image_points.pop_front();
//    cout << " detected corners! pyr " << p << endl;
  }
//  else
//    cout << " No corners detected " << endl;

  auto t2 = std::chrono::steady_clock::now();
  cout << " findCorners on image on pyr " << p << " " << std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count() << " ms" << endl;

  if( display )
  {
    cv::Mat img_display = img.clone();
    drawChessboardCorners(img_display, board_size, corners, detected);
    imshow("corners", img_display);
    waitKey(500);
  }
  return detected ? p : -1;
}

void CameraCalibrator::findCorners(const string & img_dir, int pyr, bool display)
{
  assert(board_cols > 0 && board_rows > 0 && square_size > 0.f); // Check that the physical information of a checjerboard is present
  assert(pyr >= 0);

  if(display)
    cv::namedWindow("corners", cv::WINDOW_AUTOSIZE);
  vector<int> detections_per_pyr(pyr+1, 0);

  // Measuere exucution time
  steady_clock::time_point t1 = steady_clock::now();

  vector<path> v_path;
  copy(directory_iterator(img_dir), directory_iterator(), back_inserter(v_path));
  sort(v_path.begin(), v_path.end());
  int n(0);
  for (const path & pa : v_path) // access by const reference
  {
    //cout << "extension " << pa.extension().string() << endl;
    if( pa.extension().string() != string(".jpg") && pa.extension().string() != string(".png") )
      continue;
    if (n++ % 10 != 0)
      continue;

    //cout << "img_file " << pa.string() << endl;
    img = imread(pa.string(), cv::IMREAD_COLOR);
    int p = findCorners(img, pyr, display);
    if (p != -1)
      ++detections_per_pyr[p];
  }
  img_size = img.size();
  destroyWindow("corners");
  if( !display )
  {
    steady_clock::time_point t2 = steady_clock::now();
    auto duration = duration_cast<milliseconds>( t2 - t1 ).count();
    cout << "CameraCalibrator::findCorners took " << duration/(float)1e3 << " seconds. \n";
  }

  // Print summary of corner detections
  int n_detections = std::accumulate(detections_per_pyr.begin(), detections_per_pyr.end(), 0);
  int missed = v_path.size()-n_detections;
  vector<float> detections_rate(pyr+1);
  std::transform(detections_per_pyr.begin(), detections_per_pyr.end(), detections_rate.begin(), [&v_path](int x) -> float { return x / (float)v_path.size(); });
  cout << "\n Summary of corners detected. \n Total imgs: " << v_path.size() << " detections: " << n_detections << " missed: " << missed << " \n pyramids: ";
  std::copy(detections_per_pyr.begin(), detections_per_pyr.end(), std::ostream_iterator<int>(std::cout, " / "));
  cout << " ratios ";
  std::copy(detections_rate.begin(), detections_rate.end(), std::ostream_iterator<float>(std::cout, " / "));
  cout << " missed " << (missed/(float)v_path.size()) << "\n\n";
}

double CameraCalibrator::computeReprojectionErrors(const vector<vector<Point3f> >& objectPoints,
                                                   const vector<vector<Point2f> >& imagePoints,
                                                   const vector<Mat>& rvecs, const vector<Mat>& tvecs,
                                                   const Mat& cameraMatrix , const Mat& distCoeffs,
                                                   std::vector<float>& perViewErrors, bool fisheye)
{
  vector<Point2f> imagePoints2;
  size_t totalPoints = 0;
  double totalErr = 0, err;
  perViewErrors.resize(objectPoints.size());
  for(size_t i = 0; i < objectPoints.size(); ++i ) {
    if (fisheye) {
      fisheye::projectPoints(objectPoints[i], imagePoints2, rvecs[i], tvecs[i], cameraMatrix, distCoeffs);
    } else {
      projectPoints(objectPoints[i], rvecs[i], tvecs[i], cameraMatrix, distCoeffs, imagePoints2);
    }
    err = norm(imagePoints[i], imagePoints2, NORM_L2);
    size_t n = objectPoints[i].size();
    perViewErrors[i] = (float) std::sqrt(err*err/n);
    totalErr        += err*err;
    totalPoints     += n;
  }
  return std::sqrt(totalErr/totalPoints);
}

double CameraCalibrator::calibrate(const string & img_dir, int pyr, bool display)
{
  //cout << "CameraCalibrator::calibrate()... " << endl;
  steady_clock::time_point t1 = steady_clock::now(); // Measuere exucution time

  pyr = (pyr >= 0) ? pyr : 0; // pyr must be positive

  findCorners(img_dir, pyr, display);
  std::vector< std::vector< cv::Point3f > > checkerboard_points(image_points.size(), checkerboard);

  vector< Mat > rvec, tvec;
  double rms = computeCalibration();

  if( !display )
  {
    steady_clock::time_point t2 = steady_clock::now();
    auto duration = duration_cast<milliseconds>( t2 - t1 ).count();
    cout << "CameraCalibrator::calibrate took " << duration/(float)1e3 << " seconds. \n";
  }

  return rms;
}

double CameraCalibrator::computeCalibration(size_t max_obs, float max_rms_per_view, int flag)
{
  cout << "CameraCalibrator::computeCalibration " << max_obs << " " << flag << " img size " << img_size << " cb size " << board_cols << " " << board_rows << " obs " << image_points.size() << endl;
  steady_clock::time_point t1 = steady_clock::now();
  vector< Mat > rvec, tvec;
//  std::vector< std::vector< cv::Point2f > > image_points_selection;
  std::vector< std::vector< cv::Point2f > > image_points_selection(image_points.begin(), image_points.end());
  double rms;
  if (max_obs != 0 && max_obs > image_points.size()) {
    image_points_selection.resize(max_obs);   // Select a random set of image points
    std::vector<size_t> in(image_points.size()), out;
    std::iota(in.begin(), in.end(), 0);
    out.reserve(max_obs);
    std::sample(in.begin(), in.end(), std::back_inserter(out), max_obs, std::mt19937{std::random_device{}()});
    for (size_t i = 0; i < max_obs; i++) {
      image_points_selection[i] = image_points[out[i]];
    }
  }
  std::vector< std::vector< cv::Point3f > > checkerboard_points(image_points.size(), checkerboard);
  flag = 0;
  if (fisheye) {
    flag |= fisheye::CALIB_RECOMPUTE_EXTRINSIC;
    flag |= fisheye::CALIB_CHECK_COND;
    flag |= fisheye::CALIB_FIX_SKEW;
    rms = fisheye::calibrate(checkerboard_points, image_points_selection, img_size, K, D, rvec, tvec, flag);
  } else {
    flag |= CALIB_FIX_ASPECT_RATIO;
    flag |= CALIB_FIX_K4;
    flag |= CALIB_FIX_K5;
    rms = calibrateCamera(checkerboard_points, image_points_selection, img_size, K, D, rvec, tvec, flag);
  }
  steady_clock::time_point t2 = steady_clock::now();
  cout << rms << " error. CameraCalibrator::computeCalibration took " << duration_cast<milliseconds>( t2 - t1 ).count() << " ms. \n";

  std::vector<float> perViewErrors;
  cout << " computeReprojectionErrors error: " << computeReprojectionErrors(checkerboard_points, image_points_selection, rvec, tvec, K, D, perViewErrors, fisheye) << endl;

  // Re-compute the calibration discarding views with larger error than a threshold
  if (max_rms_per_view > 0) {
    std::vector< std::vector< cv::Point2f > > image_points_low_rms;
    image_points_low_rms.reserve(image_points.size());
    for (size_t i=0; i < perViewErrors.size(); i++) {
      if (perViewErrors[i] < max_rms_per_view)
        image_points_low_rms.emplace_back(image_points[i]);
    }
    cout << "Select low rms views " << image_points_low_rms.size() << " / " << image_points.size() << endl;
    std::vector< std::vector< cv::Point3f > > checkerboard_points_low_rms(image_points_low_rms.size(), checkerboard);
    if (fisheye) {
      rms = fisheye::calibrate(checkerboard_points_low_rms, image_points_low_rms, img_size, K, D, rvec, tvec, flag);
    } else {
      rms = calibrateCamera(checkerboard_points_low_rms, image_points_low_rms, img_size, K, D, rvec, tvec, flag);
    }
    cout << " 2nd Calibration error: " << computeReprojectionErrors(checkerboard_points_low_rms, image_points_low_rms, rvec, tvec, K, D, perViewErrors, fisheye) << endl;
  }

  return rms;
}

void CameraCalibrator::setCheckerboardSize()
{
  checkerboard.clear();
  for (int i = 0; i < board_rows; i++)
    for (int j = 0; j < board_cols; j++)
      checkerboard.push_back(Point3f((float)j * square_size, (float)i * square_size, 0));
}
