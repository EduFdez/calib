# calib
Camera calibration library

## 1. Introduction
Calib contains is a set of C++ (c++11) applications to perform camera calibration: intrinsic and extrinsic (i.e. stereo calibration) using a checkerboard pattern based on OpenCV. In contrast with other open source calibration projects, this project implements an easy CLI interface for calibration from a directory of checkerboard images, with a simple display option. This project is also implemented to serve as a simple didactic guide for monocular and stereo calibration.

This project is also implemented in python (3.6), the documentation is available [here](https://gitlab.com/EduFdez/calib/tree/master/python).

## 2. Dependencies

This software has been tested in Ubuntu 18.04. It requires the following dependencies:

    * CMake >= 3.1 required
    * OpenCV >= 3.0.0 required
    * Boost Filesystem >= 1.46 required

```bash
sudo apt install build-essential pkg-config libopencv-dev libboost-dev libboost-dev
```

## 3. Compile
In order to build this project (C++), open a terminal and enter:
``` bash
cd <your_code_dir>
git clone https://gitlab.com/EduFdez/calib
mkdir build && cd build 
cmake ..
make -j
```

## 4. Usage

To use this software you will need to record a sequence of images that observe a checkerboard pattern, you can download one [here](https://www.mrpt.org/downloads/camera-calibration-checker-board_9x7.pdf). Make sure to place it on a flat surface.

### Calibration applications

```bash
./bin/calib_camera -h 
```
This program parses a directory containing checkerboard images and computes the camera calibration. The checkerboard parameters must be specified as command line arguments.

```bash
./bin/calib_stereo_cam -h 
```
This program parses the directories containing checkerboard images from a stereo camera (i.e. cam0, cam1) and computes the stereo camera calibration. The checkerboard parameters must be specified as command line arguments.

## 5. License
Calib is released under the [3-clause BSD License](https://gitlab.com/EduFdez/calib/blob/master/LICENSE).
