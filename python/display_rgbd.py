#!/usr/bin/python3.6

# Copyright (c) 2019 Author: Eduardo Fernandez-Moral
# The CALIB project is released under the 3-clause BSD License
# The License can be consulted in https://gitlab.com/EduFdez/calib/blob/master/LICENSE

import sys
import open3d as o3d
import matplotlib.pyplot as plt
from calib_stereo import StereoCalibrator
import argparse


def main():
    print(sys.version, '\nOpen3D', o3d.__version__)
    parser = argparse.ArgumentParser(description="Display RGB-D image.")
    parser.add_argument("-i", "--image", type=str, required=True, help="Input image: either color (jpg) or depth (png)")
    parser.add_argument("-c", "--calib", type=str, required=True, help="Stereo calibration file")
    args = parser.parse_args()
    print(args)

    if args.image.endswith('.jpg'):
        color = o3d.io.read_image(args.image)
        depth = o3d.io.read_image(args.image[:-4]+'.png')
    elif args.image.endswith('.png'):
        depth = o3d.io.read_image(args.image)
        color = o3d.io.read_image(args.image[:-4]+'.jpg')
    else:
        print('Input image format must be .jpg or .png')
        return

    rgbd_image = o3d.geometry.RGBDImage.create_from_color_and_depth(
        color, depth, depth_trunc=140.0, convert_rgb_to_intensity=False)

    calib = StereoCalibrator.from_file(args.calib)
    cr_mat = calib.stereo_calib.cam_rect[0].P  # calib.cam_rect[0].cam.K  # Camera Rectified Matrix
    im_size = calib.stereo_calib.cam_rect[0].cam.size
    print('Intrinsics rectified:\n', cr_mat, '\nSize:', im_size)
    intrinsic = o3d.camera.PinholeCameraIntrinsic(width=im_size[0], height=800, fx=cr_mat[0, 0], fy=cr_mat[1, 1],
                                       cx=cr_mat[0, 2], cy=cr_mat[1, 2])
    print(rgbd_image)
    plt.subplot(1, 2, 1)
    plt.title('Color image')
    plt.imshow(rgbd_image.color)
    plt.subplot(1, 2, 2)
    plt.title('Depth image')
    plt.imshow(rgbd_image.depth)
    plt.show()
    pcd = o3d.geometry.PointCloud.create_from_rgbd_image(rgbd_image, intrinsic)
    # Flip it, otherwise the pointcloud will be upside down
    pcd.transform([[1, 0, 0, 0], [0, -1, 0, 0], [0, 0, -1, 0], [0, 0, 0, 1]])
    o3d.visualization.draw_geometries([pcd])
    print('Program finished! \n')


if __name__ == "__main__":  # Main program
    main()
