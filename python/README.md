# calib

Camera calibration: intrinsic and extrinsic (stereo)

## 1. Dependencies
```bash
sudo apt install -y python3-pip
sudo pip3 install opencv-python
```

## 2. Usage

To use this software you will need to record a sequence of images that observe a checkerboard pattern, you can download one [here](https://www.mrpt.org/downloads/camera-calibration-checker-board_9x7.pdf). Make sure to place it on a flat surface.

### Calibration applications

```bash
python3 calib_camera -h 
```
This program parses a directory containing checkerboard images and computes the camera calibration. The checkerboard parameters must be specified as command line arguments.

```bash
python3 calib_stereo -h 
```
This program parses the directories containing checkerboard images from a stereo camera (i.e. cam0, cam1) and computes the stereo camera calibration. The checkerboard parameters must be specified as command line arguments.


## 3. License
Calib is released under the [3-clause BSD License](https://gitlab.com/EduFdez/calib/blob/master/LICENSE).
