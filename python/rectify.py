#!/usr/bin/python3

# Copyright (c) 2020 Author: Eduardo Fernandez-Moral
# The CALIB project is released under the 3-clause BSD License
# The License can be consulted in https://gitlab.com/EduFdez/calib/blob/master/LICENSE

from calib_camera import *


def main():
    parser = argparse.ArgumentParser(description="This program computes the intrinsic calibration of a camera "
                                     "(camera_matrix+distortion) from a sequence of images that observe a checkerboard")
    parser.add_argument("-i", "--img_dir", type=str, required=True, help="Directory containing the checkerboard images")
    parser.add_argument("-c", "--calib", type=str, required=True, help="Calibration file")
    parser.add_argument("-f", "--fisheye", dest='fisheye', action='store_true', required=False,
                        help="optional parameter to choose fisheye camera model")
    parser.add_argument("-d", "--display", dest='display', action='store_true', required=False,
                        help="optional parameter to display the detections in each images")
    parser.add_argument("-o", "--output", type=str, default='', help="Output calibration file")
    args = parser.parse_args()
    print('args', args)

    calib = CameraCalib(args.calib, args.fisheye)
    # calib.calc_rectification_map(1)
    # Display raw and rectified calibration images
    if args.display:
        images = find_images(args.img_dir)
        if len(images) == 0:
            print("No image found in the directory:", args.img_dir)
            return
        for i, file_name in enumerate(images):
            if i % 10 != 0:
                continue
            print(i, "image:", file_name)
            img_raw = cv.imread(os.path.join(args.img_dir, file_name))
            img_rect = calib.rectify(img_raw)
            img_display = np.hstack([img_raw, img_rect])
            win_name = f'{file_name} Raw & Rectified'
            if img_display.shape[0] > MAX_WIN_SIZE[1] or img_display.shape[1] > MAX_WIN_SIZE[0]:
                ratio = max(np.array(img_display.shape[:2], dtype=np.float) / np.array(MAX_WIN_SIZE[::-1]))
                win_size = tuple((np.array(img_display.shape[:2][::-1]) / ratio).astype(int).tolist())
                cv.namedWindow(win_name, cv.WINDOW_NORMAL)
                cv.resizeWindow(win_name, win_size[0], win_size[1])
            cv.imshow(win_name, img_display)

            cv.waitKey()
            cv.destroyAllWindows()


if __name__ == "__main__":  # Main program
    main()
