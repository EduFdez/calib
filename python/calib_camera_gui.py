# #!/usr/bin/python3
#
# # Copyright (c) 2018 Author: Eduardo Fernandez-Moral
# # The CALIB project is released under the 3-clause BSD License
# # The License can be consulted in https://gitlab.com/EduFdez/calib/blob/master/LICENSE
#
# # from PyQt5.QtGui import QGuiApplication
#
# from calib_camera import CalibIntrinsic

# from PyQt5.QtWidgets import QApplication, QWidget, QPushButton, QVBoxLayout
#
#     # calib = CalibIntrinsic(args.cols, args.rows, args.square_size)
#     # success = calib.calibrate(args.img_dir, args.pyr, args.display)
#     # if success:
#     #     calib.save(args.output)
#
# #
# #     # Create an instance of the application
# #     app = QGuiApplication(sys.argv)
# #     app.setApplicationName("Camera Calibration")
# #
# #     # create a worker
# #     worker = Worker()
# #
# #     # create an application engine
# #     engine = QQmlApplicationEngine()
# #     engine.load(QUrl("res/qml/Main.qml"))
# #     engine.rootContext().setContextProperty("worker", worker)
# #
# #     # register PaintedImages into the worker
# #     root = engine.rootObjects()[0]
# #     for name in ["leftImage", "rightImage"]:
# #         obj = root.findChild(PaintedImage, name)
# #         if obj is not None:
# #             worker.register_painted_image(name, obj.update_image)
# #
# #     return app.exec()
#
#
# if __name__ == "__main__":  # Main program
#     # main()
#
#     app = QApplication([])
#     window = QWidget()
#     layout = QVBoxLayout()
#     layout.addWidget(QPushButton('Top'))
#     layout.addWidget(QPushButton('Bottom'))
#     window.setLayout(layout)
#     window.show()
#     app.exec_()
#

##########################

import os
import sys
import inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, parentdir)
import pathlib
from PySide2 import QtWidgets, QtCore, QtGui  #, QtQml
# from PySide2.QtGui import QImage
# from PyQt5.QtGui import QImage
from qt_gui import mainwindow
from calib_camera import *
# from calib_camera import CalibIntrinsic
# from calib_stereo import CalibStereo


class CalibIntrinsicSignals(CameraCalibrator, QtCore.QObject):

    # cb_detection = QtCore.pyqtSignal()
    cb_detection = QtCore.Signal()  # QtCore.pyqtSignal() in PyQt

    def __init__(self, board_cols=0, board_rows=0, square_size=0):
        super().__init__(board_cols=board_cols, board_rows=board_rows, square_size=square_size)
        # CalibIntrinsic.__init__(self, board_cols, board_rows, square_size)
        # QtCore.QObject.__init__(self)

    def find_corners(self, img_dir, pyr=0, display=False):

        # termination criteria
        subpix_criteria = (cv.TERM_CRITERIA_EPS + cv.TERM_CRITERIA_MAX_ITER, 30, 1e-3)

        img_pts = {}
        detections_per_pyr = np.zeros(pyr+1, dtype=np.uint32)
        images = glob.glob(os.path.join(img_dir, '*.jpg'))
        # Check other file extensions if no jpg images are found
        if len(images) == 0:
            images = glob.glob(os.path.join(img_dir, '*.png'))
        if len(images) == 0:
            print("No image found in the directory:", img_dir)
            return img_pts
        for i, file_name in enumerate(sorted(images)):
            print('file_name', file_name)
            img = cv.imread(file_name)
            if len(img.shape) > 2 and img.shape[2] > 1:
                gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
            # Check that all the images have the same size
            if i == 0:
                self.img_size = gray.shape
            else:
                assert self.img_size == gray.shape

            # Find the chess board corners
            flags = cv.CALIB_CB_ADAPTIVE_THRESH + cv.CALIB_CB_FILTER_QUADS
            detected, corners = cv.findChessboardCorners(gray, self.checker_size, flags=flags)
            if not detected:  # Search in lower resolution images
                img_pyr = [gray]
                for p in range(pyr):
                    img_pyr.append(cv.pyrDown(img_pyr[-1]))
                    detected, corners = cv.findChessboardCorners(img_pyr[-1], self.checker_size, flags=flags)
                    if detected:
                        corners *= 2**(p+1)  # Re-scale the corner detections for the original image size
                        detections_per_pyr[p+1] += 1
                        break
            else:
                detections_per_pyr[0] += 1

            # If found, add object points, image points (after refining them)
            if detected:
                corners2 = cv.cornerSubPix(gray, corners, (11, 11), (-1, -1), subpix_criteria)
                img_pts[os.path.basename(file_name)] = corners2
                # self.imgpoints.append(corners2)

                # Draw and display the corners
                self.img_corners = cv.drawChessboardCorners(img, self.checker_size, corners2, detected)
                self.cb_detection.emit()
                # print('log_dir', self.log_dir)
                # print('out_file', os.path.join(self.log_dir + str(img_dir).split('/')[-1], file_name))
                # cv.imwrite(os.path.join(self.log_dir + img_dir.split('/')[-1], file_name), self.img_corners)
                if display:
                    # self.img_corners = cv.drawChessboardCorners(img, self.checker_size, corners2, detected)
                    cv.imshow('corners', self.img_corners)
                    cv.waitKey(500)
            else:
                self.logger.info(f' No corners detected in {file_name}')


class CalibApp(mainwindow.Ui_StereoCalibAPP, QtWidgets.QMainWindow):

    calib_s = CalibIntrinsicSignals()

    def __init__(self):
        # super().__init__(self)
        # CalibIntrinsicUI.__init__(0, 0, 0)
        mainwindow.Ui_StereoCalibAPP.__init__(self)
        QtWidgets.QMainWindow.__init__(self)
        self.setupUi(self)
        self.left_dir_TB.clicked.connect(self.browse_left_dir)
        self.right_dir_TB.clicked.connect(self.browse_right_dir)
        self.out_dir_TB.clicked.connect(self.browse_out_dir)
        self.load_PB.clicked.connect(self.load)
        self.calibrate_PB.clicked.connect(self.calibrate)

        # Connect signals from the calibration process
        # CalibIntrinsicUI.cb_detection.connect(self.display_cb_detection)
        # self.cb_detection.connect(self.display_cb_detection)

        # self.connection_cb_detection(self.calib_s)calib_s

    def display_cb_detection(self):
        self.log_PTE.appendPlainText(f'Display detection')

    # def connection_cb_detection(self, calib_object):
    #     calib_object.cb_detection.connect(self.display_cb_detection)

    # # @pyqtSlot()
    # @QtCore.Slot()
    # def display_cb_detection(self, path):
    #     self.log_PTE.appendPlainText(f'Display detection')

    def load(self):
        # Read image dir
        left_dir = pathlib.Path(self.left_dir_LE.text())
        if not left_dir.exists():
            QtWidgets.QMessageBox.about(self, 'ERROR', 'Please, provide a valid image directory')
            return
        left_images = find_images(left_dir)
        self.log_PTE.appendPlainText(f'Load images: {len(left_images)} stereo pairs')
        self.log_PTE.appendPlainText(f'image 0: {left_images[0]}')
        left_img = cv.imread(os.path.join(left_dir, left_images[0]))
        height, width, channel = left_img.shape
        bytesPerLine = 3 * width
        qt_left_image = QtGui.QImage(left_img.data, width, height, bytesPerLine, QtGui.QImage.Format_RGB888)
        # qt_left_image = QImage(left_img.GetData(), left_img.GetWidth(), left_img.GetHeight(), left_img.GetStride(), left_img.Format_RGB888)

        # Create widget
        # label = QLabel(self)
        pixmap = QtGui.QPixmap(os.path.join(left_dir, left_images[0]))
        # self.left_GV.setPixmap(pixmap)
        self.left_GV.addPixmap(pixmap)
        self.show()

        # self.resize(pixmap.width(), pixmap.height())
        self.log_PTE.appendPlainText(f'image 0: {left_images[0]}')


    def calibrate(self):
        # Read parameters
        try:
            cols = int(self.col_LE.text())
            rows = int(self.row_LE.text())
            square_size = float(self.ssize_LE.text())
        except ValueError:
            QtWidgets.QMessageBox.about(self, 'ERROR', 'Please, provide valid checkerboard dimensions')
            return
        self.log_PTE.appendPlainText(f'Checkerboard dimensions: {cols}x{rows}. Square-length {square_size:.4f} meters')
        # self.calib.set_cb_dimensions(cols, rows, square_size)

        # Read image dir
        # self.load()
        left_dir = pathlib.Path(self.left_dir_LE.text())
        if not left_dir.exists():
            QtWidgets.QMessageBox.about(self, 'ERROR', 'Please, provide a valid image directory')
            return

        # right_dir = pathlib.Path(self.right_dirr_LE.text())
        # if not right_dir.exists():
        #     QtWidgets.QMessageBox.about(self, 'ERROR', 'Please, provide a valid image directory')
        #     return

        out_dir = pathlib.Path(self.out_dir_LE.text())
        if not out_dir.exists():
            QtWidgets.QMessageBox.about(self, 'ERROR', 'Please, provide a valid directory')
            return

        pyr = 2
        # self.calib = CalibIntrinsicUI(cols, rows, square_size)
        self.calib.find_corners(left_dir, pyr)
        # # calib = CalibIntrinsic(cols, rows, square_size, out_dir)
        # success = calib.calibrate(left_dir, pyr)
        # if success:
        #     calib.save(self.out_dir_LE.text())

    def browse_left_dir(self):
        dir = QtWidgets.QFileDialog.getExistingDirectory(self, 'Left image directory')
        self.left_dir_LE.setText(dir)

    def browse_right_dir(self):
        dir = QtWidgets.QFileDialog.getExistingDirectory(self, 'Right image directory')
        self.right_dir_LE.setText(dir)

    def browse_out_dir(self):
        dir = QtWidgets.QFileDialog.getExistingDirectory(self, 'Results directory')
        self.out_dir_LE.setText(dir)


if __name__ == '__main__':
    # QtQml.QQmlDebuggingEnabler()
    app = QtWidgets.QApplication(sys.argv)
    qt_app = CalibApp()
    qt_app.show()

    # Keep Python (main) in the loop
    timer = QtCore.QTimer()
    timer.timeout.connect(lambda: None)
    timer.start(100)

    sys.exit(app.exec_())
