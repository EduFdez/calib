#!/usr/bin/python3

# Copyright (c) 2018 Author: Eduardo Fernandez-Moral
# The CALIB project is released under the 3-clause BSD License
# The License can be consulted in https://gitlab.com/EduFdez/calib/blob/master/LICENSE

import numpy as np
import random
import os
import cv2 as cv
import glob
import time
import argparse
import logging
# from typing import Type

from collections import namedtuple
CameraIntrinsics = namedtuple("CameraIntrinsics", "size K D")
CameraRectify = namedtuple("CameraRectify", "cam R P")

# Maximum window size to display
MAX_WIN_SIZE = [1500, 1200]
margin = 100
from screeninfo import get_monitors
if len(get_monitors()):
    m = get_monitors()[0]
    MAX_WIN_SIZE = [m.width - margin, m.height - margin]


def get_pose_cam2cb(rvec, tvec):
    rotation_matrix = cv.Rodrigues(rvec)[0]
    pose_cam2cb = np.identity(4, dtype=np.float64)
    pose_cam2cb[:3, :3] = rotation_matrix
    pose_cam2cb[:3, 3] = tvec.squeeze()
    return pose_cam2cb


def find_images(img_dir, ext=''):
    if not ext:
        images = glob.glob(os.path.join(img_dir, '*.jpg'))
        # Check other file extensions if no jpg images are found
        if len(images) == 0:
            images = glob.glob(os.path.join(img_dir, '*.png'))
    else:
        images = glob.glob(os.path.join(img_dir, '*.'+ext))
    # return sorted(images)
    return sorted([os.path.basename(fname) for fname in images])

def is_color_image(img: np.ndarray) -> bool:
    return len(img.shape) == 3 and img.shape[2] > 2

def is_monochrome_image(img: np.ndarray) -> bool:
    return len(img.shape) == 2 or (len(img.shape) == 3 and img.shape[2] == 1)


class CameraCalib:  # This class will substitute CameraIntrinsics, CameraRectify
    def __init__(self, calib_file='', fisheye=False):
        self.fisheye = fisheye
        self.img_size = None
        self.K = None
        self.D = None
        self.R = None
        self.P = None
        self.map = None
        if calib_file:
            self.load(calib_file)
            self.calc_rectification_map()

    def print(self):
        print("CameraCalib fisheye", self.fisheye)
        print("K", self.K)
        print("D", self.D)
        print("R", self.R)
        print("P", self.P)

    def calc_rectification_map(self, alpha=0.5):
        # self.R = np.identity(3)
        # alpha: Free scaling parameter between 0 (when all the pixels in the undistorted image are valid) and 1 (when all the source image pixels are retained in the undistorted image)
        if self.fisheye:
            self.P = cv.fisheye.estimateNewCameraMatrixForUndistortRectify(self.K, self.D, self.img_size, self.R, balance=alpha)
            self.map = cv.fisheye.initUndistortRectifyMap(self.K, self.D, self.R, self.P, self.img_size, cv.CV_32FC1)
            # self.map = cv.fisheye.initUndistortRectifyMap(self.K, self.D, self.R, self.K, self.img_size, cv.CV_32FC1)
        else:
            self.P, _ = cv.getOptimalNewCameraMatrix(self.K, self.D, self.img_size, alpha)
            # self.map = cv.initUndistortRectifyMap(self.K, self.D, self.R, self.P, self.img_size, cv.CV_32FC1)
            self.map = cv.initUndistortRectifyMap(self.K, self.D, self.R, self.K, self.img_size, cv.CV_32FC1)
        self.print()

    def rectify(self, raw):
        return cv.remap(raw, self.map[0], self.map[1], cv.INTER_LINEAR)

    def save(self, output_file='', rms=None):
        if not output_file:
            output_file = os.path.join(self.out_dir, 'cam_intrinsics.yml')
        fs = cv.FileStorage(output_file, cv.FILE_STORAGE_WRITE)
        fs.write("size", np.array(self.img_size, dtype=int))
        fs.write("K", self.K)
        fs.write("D", self.D)
        # fs.write("fisheye", self.fisheye)
        if rms:
            fs.write("RMS", rms)
        fs.release()

    def load(self, calib_file: str):
        fs = cv.FileStorage(calib_file, cv.FILE_STORAGE_READ)
        self.img_size = tuple(fs.getNode("size").mat().flatten().astype(int))  # (cols, rows)
        self.K = fs.getNode("K").mat()
        self.D = fs.getNode("D").mat()
        # self.fisheye = fs.getNode("fisheye") < .1
        self.fisheye = (self.D.size == 4)  # TODO: code this properly as non-fisheye can also be described by 4 distortion parameters
        fs.release()

    # @staticmethod
    # def load(calib_file: str) -> CameraIntrinsics:
    #     fs = cv.FileStorage(calib_file, cv.FILE_STORAGE_READ)
    #     size = fs.getNode("size").mat().astype(int)
    #     K = fs.getNode("K").mat()
    #     D = fs.getNode("D").mat()
    #     fs.release()
    #     return CameraIntrinsics(size, K, D)

    def project_points(self, xyz, rot, trans, image=None, rectified=False, display='', wait_key=0, colors=None, size=1, img_copy=True):
        from scipy.spatial.transform import Rotation as R
        if self.R:
            rot = (R.from_rotvec(rot) * R.from_matrix(self.R)).as_rotvec()
        if rectified:
            uv_xyz_from_cam, _ = cv.projectPoints(xyz, rot, trans, self.P, np.zeros((5,)))
        else:
            if self.fisheye:
                uv_xyz_from_cam, _ = cv.fisheye.projectPoints(xyz, rot, trans, self.K, self.D)
            else:
                uv_xyz_from_cam, _ = cv.projectPoints(xyz, rot, trans, self.K, self.D)

        uv_xyz_from_cam = uv_xyz_from_cam.squeeze().astype(np.int)
        points_inside = np.logical_and(np.logical_and(uv_xyz_from_cam[:, 0] >= 0, uv_xyz_from_cam[:, 1] >= 0),
                                       np.logical_and(uv_xyz_from_cam[:, 0] < self.img_size[0],
                                                      uv_xyz_from_cam[:, 1] < self.img_size[1]))
        points_inside = np.where(points_inside)[0]
        uv_xyz_from_cam = uv_xyz_from_cam[points_inside]
        img_display = image.copy() if img_copy else image
        if img_display is not None:
            for j, p in zip(points_inside, uv_xyz_from_cam):
                if colors is not None:
                    if len(colors) > 3:
                        cv.circle(img_display, (p[0], p[1]), size, tuple(255 * colors[j][::-1]), -1)
                    elif len(colors) == 3:
                        cv.circle(img_display, (p[0], p[1]), size, tuple(colors[::-1]), -1)
                else:
                    cv.circle(img_display, (p[0], p[1]), size, (255, 0, 0), -1)
        if display:
            cv.imshow(display, img_display)
            key = cv.waitKey(wait_key)
        else:
            key = 0  # Default return key
        return key, points_inside, uv_xyz_from_cam, img_display


class CameraCalibrator(CameraCalib):
    """ Compute the intrinsic calibration of a camera from the observations of a checkerboard.
    """
    def __init__(self, board_cols, board_rows, square_size, fisheye=False, calib_file='', out_dir=''):
        CameraCalib.__init__(self, calib_file=calib_file, fisheye=fisheye)
        self.cols = board_cols
        self.rows = board_rows
        self.square_size = square_size
        self.checker_size = (board_cols, board_rows)
        self.checkerboard = np.zeros((self.cols*self.rows, 3), np.float32)
        self.checkerboard[:, :2] = self.square_size * np.mgrid[0:self.cols, 0:self.rows].T.reshape(-1, 2)
        self.checkerboard = self.checkerboard[:, None]  # Necessary for fisheye calibration
        self.img_pts = {}  # dict{img_name:img_points} 2D points in image plane.

        # Calibration results
        self.out_dir = out_dir

        # Calibration display
        self.img_corners = None

        # Calibration logs
        self.logger = logging.getLogger('CalibIntrinsic')
        self.logger.setLevel(logging.DEBUG)
        # self.log_dir = os.path.join(out_dir, 'log')
        # if not os.path.isdir(self.log_dir):
        #     os.makedirs(self.log_dir)
        # fh = logging.FileHandler(os.path.join(self.log_dir, 'CalibIntrinsic.log'))
        fh = logging.FileHandler('CalibIntrinsic.log')
        fh.setLevel(logging.DEBUG)
        self.logger.addHandler(fh)
        self.logger.info("****** CalibIntrinsic ******\n")
        self.logger.info(f"OpenCV version: {cv.__version__}")

    def set_cb_dimensions(self, board_cols, board_rows, square_size):
        """ Set the checkerboard dimensions.
        """
        self.cols = board_cols
        self.rows = board_rows
        self.square_size = square_size
        self.checker_size = (board_cols, board_rows)
        self.checkerboard = np.zeros((self.cols*self.rows, 3), np.float32)
        self.checkerboard[:, :2] = self.square_size * np.mgrid[0:self.cols, 0:self.rows].T.reshape(-1, 2)

    def check_valid_detection(self, corners, flicker_threshold=4, curv_threshold=8):
        """ Verify that the corners follow lines.
        """
        # Validate columns
        for c in range(self.cols):
            i, j = (c, c+(self.rows-1)*self.cols)
            n = np.array([corners[j, 1] - corners[i, 1], corners[i, 0] - corners[j, 0]])
            n /= np.linalg.norm(n)
            C = -np.dot(n, corners[i, :])
            inner_pts = np.arange(1, self.rows)*self.cols + c
            distances = np.dot(corners[inner_pts, :], n) + C
            error_in_curve = (distances >= curv_threshold).all() != (distances <= curv_threshold).all()
            # print('c', c, 'max distance', max(abs(distances)), 'error_in_curve', error_in_curve)
            if (not error_in_curve and (max(distances)-min(distances)) > flicker_threshold) or (abs(distances) > curv_threshold).any():
                print('c', c, 'distances', max(abs(distances)), 'flicker', (max(distances)-min(distances)), 'error_in_curve', error_in_curve, distances)
                return False
        # Validate rows
        for r in range(self.rows):
            i, j = tuple([r*self.cols, (r+1)*self.cols - 1])
            n = np.array([corners[j, 1] - corners[i, 1], corners[i, 0] - corners[j, 0]])
            n /= np.linalg.norm(n)
            C = -np.dot(n, corners[i, :])
            inner_pts = np.arange(r*self.cols+1, (r+1)*self.cols - 1)
            distances = np.dot(corners[inner_pts, :], n) + C
            error_in_curve = (distances >= curv_threshold).all() != (distances <= curv_threshold).all()
            # print('r', r, 'max distance', max(abs(distances)), 'error_in_curve', error_in_curve)
            if (not error_in_curve and (max(distances)-min(distances)) > flicker_threshold) or (abs(distances) > curv_threshold).any():
                print('r', r, 'distances', max(abs(distances)), 'flicker', (max(distances)-min(distances)), 'error_in_curve', error_in_curve, distances)
                return False
        return True

    def find_cb_corners(self, img: np.ndarray, pyr=0):
        # Measure checkerboard detection time
        start_time = time.time()
        if is_color_image(img):
            gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
        elif is_monochrome_image(img):
            gray = img
        else:
            raise RuntimeError('Failed to load image')

        # Check that all the images have the same size
        self.img_size = gray.shape[::-1]

        # Find the chess board corners
        flags = cv.CALIB_CB_ADAPTIVE_THRESH + cv.CALIB_CB_FILTER_QUADS
        if self.fisheye:
            flags |= cv.CALIB_CB_FAST_CHECK
        detected, corners = cv.findChessboardCorners(gray, self.checker_size, flags=flags)
        p = 0  # pyramid level
        if not detected:  # Search in lower resolution images
            img_pyr = [gray]
            for p in range(1, pyr + 1):
                img_pyr.append(cv.pyrDown(img_pyr[-1]))
                detected, corners = cv.findChessboardCorners(img_pyr[-1], self.checker_size, flags=flags)
                if detected:
                    subpix_criteria = (cv.TERM_CRITERIA_EPS + cv.TERM_CRITERIA_MAX_ITER, 30, 1e-3)
                    corners *= 2 ** p  # Re-scale the corner detections for the original image size
                    corners = cv.cornerSubPix(gray, corners, (11, 11), (-1, -1), subpix_criteria)
                    break
        return detected, corners, p

    def cb_detect(self, img, pyr=0, display=False, confirm=False, save='', **kwargs):
        # Measure checkerboard detection time
        start_time = time.time()

        if display or confirm:
            tittle = 'Confirm the detection is correct [y] or discard it [space]' if confirm else 'corners'  # str(int(file_name.split('.')[0])) + ' ' +
            win_size = tuple(MAX_WIN_SIZE)
            cv.namedWindow(tittle, cv.WINDOW_NORMAL)  # WINDOW_AUTOSIZE | WINDOW_NORMAL
            # cv.resizeWindow(tittle, 1200, 700)  # WINDOW_AUTOSIZE | WINDOW_NORMAL
        detections_per_pyr = np.zeros(pyr+1, dtype=np.uint32)

        if is_color_image(img):
            gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
        elif is_monochrome_image(img):
            gray = img
        else:
            raise RuntimeError('Failed to load image')
        self.img_size = gray.shape

        # Find the chess board corners
        flags = cv.CALIB_CB_ADAPTIVE_THRESH + cv.CALIB_CB_FILTER_QUADS # + cv.CALIB_CB_FAST_CHECK
        if 'flags' in kwargs:
            flags = kwargs['flags']
        detected, corners = cv.findChessboardCorners(gray, self.checker_size, flags=flags)
        p = 0  # pyramid level
        if not detected:  # Search in lower resolution images
            img_pyr = [gray]
            for p in range(1, pyr+1):
                img_pyr.append(cv.pyrDown(img_pyr[-1]))
                detected, corners = cv.findChessboardCorners(img_pyr[-1], self.checker_size, flags=flags)
                if detected:
                    corners *= 2**p  # Re-scale the corner detections for the original image size
                    break
        if detected:
            detections_per_pyr[p] += 1

        # If found, add object points, image points (after refining them)
        if detected:
            subpix_criteria = (cv.TERM_CRITERIA_EPS + cv.TERM_CRITERIA_MAX_ITER, 30, 1e-3)
            corners2 = cv.cornerSubPix(gray, corners, (11, 11), (-1, -1), subpix_criteria)
            valid = self.check_valid_detection(corners2.squeeze())
            if display or confirm or save:
                self.img_corners = cv.drawChessboardCorners(img, self.checker_size, corners2, detected)
                cv.putText(self.img_corners, f'valid {valid}', (10, 20), cv.FONT_HERSHEY_SIMPLEX, 0.8, (0, 255*int(valid), 255*int(not valid)), 2)
            if display or confirm:
                # cv.imwrite(os.path.join(self.log_dir + img_dir.split('/')[-1], file_name), self.img_corners)
                # if self.img_corners.shape[0] > MAX_WIN_SIZE[1] or self.img_corners.shape[1] > MAX_WIN_SIZE[0]:
                #     ratio = max(np.array(self.img_corners.shape[:2], dtype=np.float) / np.array(MAX_WIN_SIZE[::-1]))
                #     win_size = tuple((np.array(self.img_corners.shape[:2][::-1])/ratio).astype(int).tolist())
                #     # self.img_corners = cv.resize(self.img_corners, win_size)
                # else:
                #     win_size = self.img_corners.shape[:2][::-1]
                ratio = max(np.array(self.img_corners.shape[:2], dtype=np.float) / np.array(MAX_WIN_SIZE[::-1]))
                win_size = tuple((np.array(self.img_corners.shape[:2][::-1]) / ratio).astype(int).tolist())
                # print('win_size', win_size)
                cv.imshow(tittle, self.img_corners)
                cv.resizeWindow(tittle, win_size[0], win_size[1])
                # cv.moveWindow(tittle, 0, 0)
                if not confirm:
                    cv.waitKey(1)
                else:
                    key = cv.waitKey()
                    if key not in [ord('y'), ord('Y')]:
                        print('Reject detection')
                        return None
            if not confirm:
                # if not self.check_valid_detection(corners2.squeeze()):
                if not valid:
                    print('Invalid detection')
                    if display:
                        cv.waitKey(1)
                    return None
            if save:
                cv.imwrite(save + '.jpg', self.img_corners)
                np.save(save + '.npy', corners2)
            return corners2
            # self.imgpoints.append(corners2)
        else:
            self.logger.info(f' No corners detected in by cb_detect')
            return None

    def draw_corners(self, img: np.ndarray, corners, detected=True, win_name='Corners', save='', text=''):
        img_corners = img.copy() if (len(img.shape) == 3 and img.shape[2] > 2) else cv.cvtColor(img, cv.COLOR_GRAY2BGR)
        cv.drawChessboardCorners(img_corners, self.checker_size, corners, detected)
        # cv.imwrite(os.path.join(self.log_dir + img_dir.split('/')[-1], file_name), img_corners)
        if img_corners.shape[0] > MAX_WIN_SIZE[1] or img_corners.shape[1] > MAX_WIN_SIZE[0]:
            ratio = max(np.array(img_corners.shape[:2], dtype=np.float) / np.array(MAX_WIN_SIZE[::-1]))
            win_size = tuple((np.array(img_corners.shape[:2][::-1]) / ratio).astype(int).tolist())
            img_corners = cv.resize(img_corners, win_size)
        else:
            win_size = img_corners.shape[:2][::-1]
        if save:
            cv.imwrite(save, img_corners)
        if text:
            cv.putText(img_corners, text, (10, 20), cv.FONT_HERSHEY_SIMPLEX, 0.8, (0, 0, 255), 2)
        cv.imshow(win_name, img_corners)
        cv.resizeWindow(win_name, win_size[0], win_size[1])
        # cv.waitKey()

    def find_corners(self, img_dir, pyr=0, display=False, confirm=False):
        # Measure checkerboard detection time
        start_time = time.time()

        img_pts = {}
        if display or confirm:
            win_name = 'Confirm the detection is correct [y] or discard it [space]' if confirm else 'corners'  # str(int(file_name.split('.')[0])) + ' ' +
            cv.namedWindow(win_name, cv.WINDOW_NORMAL)  # WINDOW_AUTOSIZE | WINDOW_NORMAL
        if confirm:
            rejected = []
            rejected_per_pyr = np.zeros(pyr + 1, dtype=np.uint32)
        detections_per_pyr = np.zeros(pyr+1, dtype=np.uint32)
        images = find_images(img_dir)
        if len(images) == 0:
            print("No image found in the directory:", img_dir)
            return img_pts
        for i, file_name in enumerate(images):
            # if i % 5 != 0:
            #     continue
            print(i, 'file_name', os.path.join(img_dir, file_name))
            img = cv.imread(os.path.join(img_dir, file_name))
            detected, corners, p = self.find_cb_corners(img, pyr)
            if detected:
                detections_per_pyr[p] += 1

            # If found, add object points, image points (after refining them)
            if detected:
                np.savetxt(f'{img_dir}/{file_name[:-4]}.txt', corners.squeeze())
                if display or confirm:
                    self.draw_corners(img, corners, detected, win_name=win_name, save=file_name)
                    if not confirm:
                        cv.waitKey(500)
                    else:
                        key = cv.waitKey()
                        if key not in [ord('y'), ord('Y')]:
                            print('Reject detection', file_name)
                            rejected.append(file_name)
                            rejected_per_pyr[p] += 1
                            continue

                # if not confirm:
                #     if not self.check_valid_detection(corners.squeeze()):
                #         print('Invalid detection')
                #         if display:
                #             cv.waitKey()
                #         continue
                img_pts[file_name.split('.')[0]] = corners
                # self.imgpoints.append(corners)
            else:
                self.logger.info(f' No corners detected in {file_name}')

        # Print summary of corner detections
        n_detections = len(img_pts.values())
        print(f"\n *** Summary of corners detected: *** \n Total images: {len(images)} ", end='')
        print(f"detect/missed: {n_detections} / {len(images)-n_detections}")
        print(f" Ratios {n_detections/len(images):.2f} / {(len(images)-n_detections)/len(images):.2f}")
        if confirm:
            print(f" Rejected detections: {len(rejected)}, ratio {float(len(rejected))/n_detections} \n")
            print(f" Rejected detections: {rejected_per_pyr} ratios: {rejected_per_pyr / len(images)} \n")
        if not display:
            print(f"CalibIntrinsic.findCorners images {len(images)} took {(time.time() - start_time):.2f} seconds ")
        else:
            cv.destroyAllWindows()

        self.img_pts = img_pts
        return img_pts

    def compute_reprojection_error(self, camera_matrix, dist_coefs, rvec, tvec, img_points, img_dir=''):
        img_names = list(img_points.keys())
        img_points = list(img_points.values())
        n_detections = len(img_points)
        error_per_view = np.zeros(n_detections)
        for i in range(n_detections):
            if self.fisheye:
                proj_pts, _ = cv.fisheye.projectPoints(self.checkerboard, rvec[i], tvec[i], camera_matrix, dist_coefs)
            else:
                proj_pts, _ = cv.projectPoints(self.checkerboard, rvec[i], tvec[i], camera_matrix, dist_coefs)
            # error_pts = cv.norm(img_points[i], proj_pts, cv.NORM_L2)
            error_pts = np.sum(np.square(np.squeeze(img_points[i] - proj_pts)), axis=1)
            error_per_view[i] = np.sum(error_pts) / len(error_pts)
        error = np.sqrt(np.sum(error_per_view) / n_detections)

        if img_dir:  #display:  # Display the image with the largest error: it may show a wrong detection
            # print('error_per_view', error_per_view)
            index_max = np.argmax(error_per_view)
            # # index_max = max(stats.iteritems(), key=operator.itemgetter(1))[0]
            # print('Max reprojection error', index_max, error_per_view[index_max], img_names[index_max])
            # print(os.path.join(img_dir, str(img_names[index_max]) + '.jpg'))
            img = cv.imread(os.path.join(img_dir, str(img_names[index_max]) + '.jpg'))
            img_corners = cv.drawChessboardCorners(img, self.checker_size, img_points[index_max], True)
            cv.imshow(f'rms {error_per_view[index_max]}', img_corners)
            # cv.resizeWindow(f'rms {error_per_view[index_max]}', tuple(img_corners.shape[:2][::-1]))
            cv.resizeWindow(f'rms {error_per_view[index_max]}', img_corners.shape[1], img_corners.shape[0])
            cv.waitKey()

        return error, error_per_view

    def compute_calibration(self, max_obs=0, max_rms_per_view=0):
        start_time = time.time()  # Measure calibration time
        if len(self.img_pts) == 0:
            return
        if 0 < max_obs < len(self.img_pts):
            img_pts = {k: self.img_pts[k] for k in random.sample(self.img_pts.keys(), max_obs)}
        else:
            img_pts = self.img_pts

        img_points = list(img_pts.values())
        obj_points = [self.checkerboard]*len(img_points)
        if self.fisheye:
            # error_idx = 39
            # img_points = img_points[:error_idx] + img_points[error_idx+1:]
            # obj_points = obj_points[:-1]
            # img_file_error = list(img_pts.keys())[error_idx]
            # print('img_file_error', img_file_error)
            # img_pts.pop(img_file_error, None)
            flags = cv.fisheye.CALIB_RECOMPUTE_EXTRINSIC + cv.fisheye.CALIB_CHECK_COND + cv.fisheye.CALIB_FIX_SKEW
            rms, self.K, self.D, rvec, tvec = cv.fisheye.calibrate(obj_points, img_points, self.img_size,
                                                                   None, None, flags=flags)
        else:
            flags = cv.CALIB_FIX_K4 + cv.CALIB_FIX_K5 + cv.CALIB_FIX_ASPECT_RATIO
            # termination_criteria = (cv.TERM_CRITERIA_EPS + cv.TERM_CRITERIA_MAX_ITER, 30, 1e-3)
            rms, self.K, self.D, rvec, tvec = cv.calibrateCamera(obj_points, img_points, self.img_size,
                                                                 None, None, flags=flags)  #, criteria=termination_criteria)

        error, error_per_view = self.compute_reprojection_error(self.K, self.D, rvec, tvec, img_pts)
        if max_rms_per_view > 0:  # Re-compute the calibration discarding views with larger error than a threshold
            indices = [i for i in range(len(error_per_view)) if error_per_view[i] < max_rms_per_view]
            img_points = img_points[indices]
            obj_points = [self.checkerboard] * len(img_points)
            if self.fisheye:
                rms, self.K, self.D, rvec, tvec = cv.fisheye.calibrate(obj_points, img_points, self.img_size,
                                                                       None, None, flags=flags)
            else:
                rms, self.K, self.D, rvec, tvec = cv.calibrateCamera(obj_points, img_points, self.img_size,
                                                                     None, None, flags=flags)  #, criteria=termination_criteria)

            error, error_per_view = self.compute_reprojection_error(self.K, self.D, rvec, tvec, img_pts)

        print(f"Calibration RMS {rms} {error}, took {(time.time() - start_time):.2f} seconds.")
        self.print()
        return rms

    def calibrate(self, img_dir, pyr=0, display=False, confirm=False, img_pts=None, max_obs=0, max_rms_per_view=0):
        """ Compute the intrinsic camera calibration.

            Args:
               img_dir (str):   Directory containing the checkerboard images
               pyr (int):       Number of pyramidal levels to use
               display (bool):  Display the checkerboard detections
               confirm (bool):  Confirm correctness of checkerboard detections
        """
        if not img_pts:
            img_pts = self.find_corners(img_dir, pyr, display, confirm)
        if len(img_pts) < 3:
            print(f"Calibration failed! Checkerboard detections {len(img_pts)}\n")
            return False, None

        rms = self.compute_calibration(max_obs, max_rms_per_view)
        return True, rms

    @staticmethod
    def draw_cb_ref(img, corners, rvec, tvec, cam_mtx, cam_dist, length=0.3):
        """ Project a reference system to the image plane
        :param img:
        :param corners:
        :param rvec:
        :param tvec:
        :param cam_mtx:
        :param cam_dist:
        :param length:
        :return:
        """
        axis = np.float32([[length, 0, 0], [0, length, 0], [0, 0, -length]]).reshape(-1, 3)
        imgpts, jac = cv.projectPoints(axis, rvec, tvec, cam_mtx, cam_dist)
        # print('imgpts', imgpts)
        corner = tuple(corners[0].ravel())
        img = cv.line(img, corner, tuple(imgpts[0].ravel()), (255, 0, 0), 5)
        img = cv.line(img, corner, tuple(imgpts[1].ravel()), (0, 255, 0), 5)
        img = cv.line(img, corner, tuple(imgpts[2].ravel()), (0, 0, 255), 5)
        return img

    @staticmethod
    def draw_lines(img, cam_mtx, cam_dist, end_points, color=(0, 0, 0)):
        """ Project a reference system to the image plane
        :param img:
        :param corners:
        :param rvec:
        :param tvec:
        :param cam_mtx:
        :param cam_dist:
        :param length:
        :return:
        """
        for ep in end_points:
            print('ep', ep)
            pts = np.float32([ep[0], ep[1]]).reshape(-1, 3)
            imgpts, jac = cv.projectPoints(pts, cam_mtx, cam_dist)
            print('imgpts', imgpts)
            img = cv.line(img, tuple(imgpts[0].ravel()), tuple(imgpts[1].ravel()), color, 5)
        return img


def main():
    parser = argparse.ArgumentParser(description="This program computes the intrinsic calibration of a camera "
                                                 "(camera_matrix+distortion) from a sequence of images that observe a checkerboard")
    parser.add_argument("-i", "--img_dir", type=str, required=True, help="Directory containing the checkerboard images")
    parser.add_argument("-c", "--cols", type=int, required=True, help="Checkerboard width: number of interior columns")
    parser.add_argument("-r", "--rows", type=int, required=True, help="Checkerboard height: number of interior rows")
    parser.add_argument("-s", "--square_size", type=float, required=True, help="Checkerboard square's width (meters)")
    parser.add_argument("-p", "--pyr", type=int, default=0,
                        help="optional parameter to set the number of pyramidal levels to use")
    parser.add_argument("-f", "--fisheye", dest='fisheye', action='store_true', required=False,
                        help="optional parameter to choose fisheye camera model")
    parser.add_argument("-S", "--select", type=int, default=1,
                        help="optional parameter to select a fraction 1/S of input frames")
    parser.add_argument("-C", "--confirm", dest='confirm', action='store_true', required=False,
                        help="optional parameter to confirm each detection manually")
    parser.add_argument("-d", "--display", dest='display', action='store_true', required=False,
                        help="optional parameter to display the detections in each images")
    parser.add_argument("-D", "--display_results", dest='display_results', action='store_true', required=False,
                        help="optional parameter to display the useful detections in each image")
    parser.add_argument("-o", "--output", type=str, default='', help="Output calibration file")
    args = parser.parse_args()
    print('args', args)

    calib = CameraCalibrator(args.cols, args.rows, args.square_size, fisheye=args.fisheye)
    success, rms = calib.calibrate(args.img_dir, args.pyr, args.display, confirm=args.confirm, max_obs=100)
    if success:
        if args.output:
            calib.save(args.output, rms)

        # Display raw and rectified calibration images
        if args.display_results:
            calib.calc_rectification_map()
            for fname, corners in calib.img_pts.items():
                img_raw = cv.imread(os.path.join(args.img_dir, fname + '.jpg'))
                img_rect = calib.rectify(img_raw)
                cv.imshow(f'{fname} Raw & Rectified', np.hstack([img_raw, img_rect]))
                cv.waitKey()
                cv.destroyAllWindows()
            # for i, (fname, corners) in enumerate(calib.img_pts.items()):
            #     if i % args.select == 0:
            #         print(i, S)
            #         img_raw = cv.imread(os.path.join(args.img_dir, fname))
            #         img_rect = calib.rectify(img_raw)
            #         cv.imshow(f'{fname} Raw & Rectified', np.hstack([img_raw, img_rect]))
            #         cv.waitKey(500)


if __name__ == "__main__":  # Main program
    main()
