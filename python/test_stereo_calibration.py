#!/usr/bin/python3.6

# Copyright (c) 2019 Author: Eduardo Fernandez-Moral
# The CALIB project is released under the 3-clause BSD License
# The License can be consulted in https://gitlab.com/EduFdez/calib/blob/master/LICENSE

from calib_stereo import StereoCalibrator
import argparse


def main():
    parser = argparse.ArgumentParser(description="This program computes the intrinsic calibration of a camera "
                                     "(camera_matrix+distortion) from a sequence of images that observe a checkerboard")
    parser.add_argument("-L", "--img_dir_left", type=str, required=True, help="Directory containing the left-cam images")
    parser.add_argument("-R", "--img_dir_right", type=str, required=True, help="Directory containing the right-cam images")
    parser.add_argument("-D", "--img_dir_disparity", type=str, required=True, help="Directory containing the  the disparity images")
    parser.add_argument("-C", "--calib", type=str, required=True, help="Stereo calibration file")
    parser.add_argument("-mto", "--max_ts_offset", type=int, default=0, help="Maximum stereo offset among left / right images")
    args = parser.parse_args()
    print('args', args)

    calib = StereoCalibrator.from_file(args.calib)
    calib.verify_disparity((args.img_dir_left, args.img_dir_right), args.img_dir_disparity)


if __name__ == "__main__":  # Main program
    main()
