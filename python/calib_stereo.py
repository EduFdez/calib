#!/usr/bin/python3.6

# Copyright (c) 2018 Author: Eduardo Fernandez-Moral
# The CALIB project is released under the 3-clause BSD License
# The License can be consulted in https://gitlab.com/EduFdez/calib/blob/master/LICENSE

from calib_camera import *

StereoCalib = namedtuple("StereoCalib", "cam_rect R T E F Q")  #, defaults=(None,)*5) # Defaults for python3.7


def find_stereo_images(img_dir_left, img_dir_right, ext=''):
    imgs_left = find_images(img_dir_left, ext)
    imgs_right = find_images(img_dir_right, ext)

    pairs = set()
    for name_left in imgs_left:
        if name_left in imgs_right:
            pairs.append(name_left)
    return pairs


def get_timestamp_pairs(ts1, ts2, max_ts_diff=1000):
    """ Get timestamp pairs from 2 input lists of nearby timestamp, and a given matching threshold """
    print('get_timestamp_pairs', len(ts1), len(ts2))
    assert len(ts1) and len(ts2)
    pairs = []
    i2 = 0
    for i1, t1 in enumerate(ts1):
        while i2 < len(ts2) and t1 > ts2[i2] + max_ts_diff:
            i2 += 1
        if i2 >= len(ts2):
            break
        if abs(t1 - ts2[i2]) < max_ts_diff:
            pairs.append((t1, ts2[i2]))
            i2 += 1
            if i2 >= len(ts2):
                break
    return pairs


class StereoCalibrator(CameraCalibrator):
    """ Compute the intrinsic calibration of a stereo camera from the observations of a checkerboard.
    """
    def __init__(self, board_cols, board_rows, square_size, out_dir=''):
        super().__init__(board_cols, board_rows, square_size, out_dir)
        # Calibration results
        self.stereo_calib: Type[StereoCalib]  # type hint as specified by PEP 484 and PEP 526 (requires python 3.5)
        self.maps = [[]]*2
        self.pairs = set()  # The pairs of images with detected checkerboards

    @classmethod
    def from_file(cls, calib_file):
        calib = cls(0, 0, 0)
        calib.stereo_calib = StereoCalibrator.load(calib_file)
        calib.calc_rectification_maps()
        return calib

    def calibrate(self, img_dir, pyr=0, display=False, confirm=False, max_ts_offset=0):
        """ Compute the intrinsic camera calibration.

            Args:
               img_dir (str):   Directory containing the checkerboard images
               pyr (int):       Number of pyramidal levels to use
               display (bool):  Display the checkerboard detections
               confirm (bool):  Let the user confirm if the detection is valid
               max_ts_offset (bool):  Maximum difference of timestamps in a stereo frame
        """
        start_time = time.time()  # Measure calibration time

        img_pts = [{}]*2
        good_calib = [False]*2
        rms = [None]*3
        intrinsics = [[]]*2
        for i in range(2):
            img_pts[i] = self.find_corners(img_dir[i], pyr, display, confirm)
            good_calib[i], rms[i] = super().calibrate(img_dir[i], pyr, display, img_pts=img_pts[i])
            intrinsics[i] = CameraIntrinsics(**self.cam._asdict())
            if not good_calib[i]:
                print("Calibration failed! Could not compute the intrinsic params \n")
                return False

        # Find the image pairs with checkerboard detections
        if max_ts_offset:
            self.pairs = get_timestamp_pairs(sorted([int(fname.split('.')[0]) for fname in img_pts[0].keys()]),
                                             sorted([int(fname.split('.')[0]) for fname in img_pts[1].keys()]), max_ts_offset)
            print('pairs diff\n', [abs(pair[0] - pair[1]) for pair in self.pairs])
        else:
            self.pairs = [(file_name, file_name) for file_name in img_pts[0].keys() if file_name in img_pts[1]]
            # self.pairs = {file_name for file_name in img_pts[0].keys() if file_name in img_pts[1]}
        if len(self.pairs) == 0:
            print("No pairs found in the directories:", img_dir[0], img_dir[1])
            return False
        imgpoints = [[img_pts[i][str(fname[i])] for fname in self.pairs] for i in range(2)]
        # imgpoints = [[img_pts[i][fname] for fname in self.pairs] for i in range(2)]
        objpoints = [self.checkerboard]*len(self.pairs)

        flags = 0
        flags |= cv.CALIB_FIX_INTRINSIC
        # flags |= cv.CALIB_FIX_PRINCIPAL_POINT
        # flags |= cv.CALIB_USE_INTRINSIC_GUESS
        # flags |= cv.CALIB_FIX_FOCAL_LENGTH
        # flags |= cv.CALIB_FIX_ASPECT_RATIO
        # flags |= cv.CALIB_ZERO_TANGENT_DIST
        # flags |= cv.CALIB_RATIONAL_MODEL
        # flags |= cv.CALIB_SAME_FOCAL_LENGTH
        # flags |= cv.CALIB_FIX_K3
        # flags |= cv.CALIB_FIX_K4
        # flags |= cv.CALIB_FIX_K5

        # Termination criteria for the calibration's optimization
        stereocalib_criteria = (cv.TERM_CRITERIA_MAX_ITER + cv.TERM_CRITERIA_EPS, 30, 1e-3)

        rms[2], K1, D1, K2, D2, R, T, E, F = \
            cv.stereoCalibrate(objpoints, imgpoints[0], imgpoints[1], intrinsics[0].K, intrinsics[0].D, intrinsics[1].K,
                               intrinsics[1].D, intrinsics[0].size, criteria=stereocalib_criteria, flags=flags)
        print(f"\nStereo Calibration RMS {rms}, took {(time.time() - start_time):.2f} seconds, pairs {len(self.pairs)}")
        intrinsics[0] = CameraIntrinsics(intrinsics[0].size, K1, D1)
        intrinsics[1] = CameraIntrinsics(intrinsics[1].size, K2, D2)
        # camera_model = dict([('K1', K1), ('K2', K2), ('D1', D1), ('D2', D2), ('R', R), ('T', T), ('E', E), ('F', F)])

        R1, R2, P1, P2, Q, validROI1, validROI2 = cv.stereoRectify(K1, D1, K2, D2, intrinsics[0].size, R, T) # flags=cv2.CALIB_ZERO_DISPARITY, newImageSize=new_size)
        print(f"\nStereo Calibration and Rectification, took {(time.time() - start_time):.2f} seconds.")

        self.stereo_calib = StereoCalib([CameraRectify(intrinsics[0], R1, P1), CameraRectify(intrinsics[1], R2, P2)], R, T, E, F, Q)

        return True, rms

    def calc_rectification_maps(self):
        calib = self.stereo_calib
        size = calib.cam_rect[0].cam.size
        R1, R2, P1, P2, Q, validROI1, validROI2 = \
            cv.stereoRectify(calib.cam_rect[0].cam.K, calib.cam_rect[0].cam.D,
                             calib.cam_rect[1].cam.K, calib.cam_rect[1].cam.D,
                             size, calib.R, calib.T)  # flags=cv2.CALIB_ZERO_DISPARITY, newImageSize=new_size)
        # Store the calibration in a StereoCalib namedtuple
        calib = StereoCalib([CameraRectify(CameraIntrinsics(size, calib.cam_rect[0].cam.K, calib.cam_rect[0].cam.D), R1, P1),
                             CameraRectify(CameraIntrinsics(size, calib.cam_rect[1].cam.K, calib.cam_rect[1].cam.D), R2, P2)],
                            calib.R, calib.T, calib.E, calib.F, calib.Q)
        for i in range(2):
            self.maps[i] = cv.initUndistortRectifyMap(calib.cam_rect[i].cam.K, calib.cam_rect[i].cam.D,
                                                      calib.cam_rect[i].R, calib.cam_rect[i].P, size, cv.CV_32FC1) # cv.CV_16SC2 / cv.CV_32FC1

    def rectify(self, raw_pair):
        rect = []  # Rectified images
        for i in range(2):
            rect.append(cv.remap(raw_pair[i], self.maps[i][0], self.maps[i][1], cv.INTER_LINEAR))
        return rect

    @staticmethod
    def display_epi_lines(img_pair, n_epi_lines=20):
        img_display = [[]]*2
        for i in range(2):
            if len(img_pair[i].shape) < 3 or img_pair[i].shape[2] == 1:
                img_display[i] = cv.cvtColor(img_pair[i], cv.COLOR_GRAY2BGR)
            else:
                img_display[i] = img_pair[i].copy()
            for line in range(1, n_epi_lines):  # Superimpose horizontal (Epipolar) lines
                img_display[i][int(line * img_display[i].shape[0] / n_epi_lines), :] = (255, 0, 0)
        cv.imshow('Epipolar lines', np.hstack([img_display[0], img_display[1]]))
        return cv.waitKey(0)

    def show_rectified(self, img_dir, ext='jpg', n_epi_lines=20, max_ts_offset=0):
        name_digits = len(os.listdir(img_dir[0])[0].split('.')[0])
        self.calc_rectification_maps()
        if max_ts_offset:
            self.pairs = get_timestamp_pairs(sorted([int(os.path.basename(fname)
                                                         ) for fname in glob.glob(os.path.join(img_dir[0], '*.') + ext)]),
                                             sorted([int(os.path.basename(fname).split('.')[0]) for fname in glob.glob(os.path.join(img_dir[1], '*.') + ext)]),
                                             max_ts_offset)
            # print('pairs diff\n', [abs(pair[0] - pair[1]) for pair in self.pairs])
        else:
            images = [[]]*2
            for i in range(2):
                images[i] = {int(os.path.basename(fname).split('.')[0]) for fname in glob.glob(os.path.join(img_dir[i], '*.') + ext)}
                self.pairs = [(im, im) for im in images[0].intersection(images[1])]
        for file_name in self.pairs:
            self.logger.info(f' Verify calibration on image {file_name}')
            raw = [[]]*2
            rect = [[]]*2
            for i in range(2):
                raw[i] = cv.imread(os.path.join(img_dir[i], str(file_name[i]).zfill(name_digits) + '.' + ext))
                # print('raw', type(raw[i]), os.path.isfile(os.path.join(img_dir[i], str(file_name[i]).zfill(name_digits) + '.' + ext)))
                cv.imshow('raw', raw[i])
                rect[i] = cv.remap(raw[i], self.maps[i][0], self.maps[i][1], cv.INTER_LINEAR)
            key = self.display_epi_lines(rect, n_epi_lines)
            if key in [27, ord('q'), ord('Q')]:
                break

    @staticmethod
    def display_disparity(img_pair, disparity, invalid_disp, calib):
        depth_factor = -(calib.cam_rect[0].P[0, 0] * calib.T[0, 0])
        def on_click_disparity(event, x, y, flags, params):
            if event == cv.EVENT_MBUTTONDOWN:  #EVENT_LBUTTONDOWN | EVENT_RBUTTONDOWN
                print("Left click", x, y, disparity.shape)
                if invalid_disp[y, x] or x >= disparity.shape[-1]:
                    print('Invalid disparity')
                    return
                for i in range(2):
                    img_display[y, :] = (255, 0, 0)
                img_display[y-5:y+5, x] = (0, 0, 255)
                corresp_x = x - int(disparity[y, x])
                img_display[y-5:y+5, disparity.shape[-1] + corresp_x, :] = (0, 0, 255)
                depth = depth_factor / disparity[y, x]
                # print("corresp_x", corresp_x, 'disp', int(disparity[y, x]), 'depth', depth)
                cv.putText(img_display, f'{depth:.1f}', (x, y), cv.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0))
                cv.imshow("Disparity tester on-click", img_display)

        key = 0
        img_display = img_pair.copy()  # [img.copy() for im in img_pair]
        disparity_display = disparity.astype(np.uint8)
        disparity_display[invalid_disp] = 0
        # cv.namedWindow("Disparity tester on-click")
        cv.imshow("Disparity tester on-click", img_display)
        cv.setMouseCallback("Disparity tester on-click", on_click_disparity)
        # cv.imshow("Disparity", disparity_display)
        # cv.setMouseCallback("Disparity", on_click_disparity)
        while key not in [27, ord('q'), ord('Q'), 13, ord(' '), 50, 52, 54, 56]:
            key = cv.waitKey(0)
            # print('key', key, chr(key))
            if key in [ord('c'), ord('C')]:
                img_display = img_pair.copy()  # [img.copy() for im in img_pair]

        cv.destroyAllWindows()
        return key

    def verify_disparity(self, img_dir, disp_dir, depth_dir='', ext='jpg', n_epi_lines=20):
        print("\n **** verify_disparity **** \n")
        disp_factor = 255 / 65534
        # self.calc_rectification_maps()
        images = [[]] * 2
        for i in range(2):
            images[i] = {os.path.basename(fname).split('.')[0] for fname in glob.glob(os.path.join(img_dir[i], '*.') + ext)}
        # stereo_pairs = sorted(images[0] & images[1])
        stereo_pairs = sorted(list(images[0].intersection(images[1])))
        print('number of pairs:', len(stereo_pairs))
        j = 0
        while True:
            file_name = stereo_pairs[j]
            print(j, file_name)
        # for file_name in stereo_pairs:
            self.logger.info(f' Verify calibration on image {file_name}')
            disparity_file = os.path.join(disp_dir, file_name + '.png')
            if not os.path.isfile(disparity_file):
                self.logger.info(f' Disparity image not available {file_name}')
                continue
            disp_scaled = cv.imread(disparity_file, cv.IMREAD_UNCHANGED)
            invalid_disp = disp_scaled == 65535
            disparity = disp_scaled * disp_factor
            # raw = [0] * 2
            rect = [0] * 2
            for i in range(2):
                rect[i] = cv.imread(os.path.join(img_dir[i], file_name + '.' + ext))
                # raw[i] = cv.imread(os.path.join(img_dir[i], file_name + '.' + ext))
                # rect[i] = cv.remap(raw[i], self.maps[i][0], self.maps[i][1], cv.INTER_LINEAR)
            img_pair = np.concatenate((rect[0], rect[1]), axis=1)
            key = self.display_disparity(img_pair, disparity, invalid_disp, self.stereo_calib)
            # print('key', key)
            if key in [27, ord('q'), ord('Q')]:
                break
            elif key in [13, ord(' ')]:
                j = (j + 1) % len(stereo_pairs)
            elif key == 50:
                j = (j - len(stereo_pairs)//10 + len(stereo_pairs)) % len(stereo_pairs)
            elif key == 52:
                j = (j - 5 + len(stereo_pairs)) % len(stereo_pairs)
            elif key == 54:
                j = (j + 5) % len(stereo_pairs)
            elif key == 56:
                j = (j + len(stereo_pairs)//10) % len(stereo_pairs)


    def save(self, output_file, rms=None):
        calib = self.stereo_calib
        fs = cv.FileStorage(output_file, cv.FILE_STORAGE_WRITE)
        fs.write("size", calib.cam_rect[0].cam.size)
        fs.write("K1", calib.cam_rect[0].cam.K)
        fs.write("K2", calib.cam_rect[1].cam.K)
        fs.write("D1", calib.cam_rect[0].cam.D)
        fs.write("D2", calib.cam_rect[1].cam.D)
        fs.write("R", calib.R)
        fs.write("T", calib.T)
        fs.write("E", calib.E)
        fs.write("F", calib.F)
        # Rectification parameters
        fs.write("R1", calib.cam_rect[0].R)
        fs.write("R2", calib.cam_rect[1].R)
        fs.write("P1", calib.cam_rect[0].P)
        fs.write("P2", calib.cam_rect[1].P)
        fs.write("Q", calib.Q)
        # Residuals quadratic mean
        if rms:
            fs.write("RMS", np.array(rms))
        fs.release()

    @staticmethod
    def load(calib_file: str):
        print('load', calib_file)
        fs = cv.FileStorage(calib_file, cv.FILE_STORAGE_READ)
        cam_rect = [None]*2
        for i in range(2):
            # size = tuple([int(fs.getNode("size").at(j).real()) for j in (range(fs.getNode("size").size()))]) # Read using the same format as in the C++ version
            size = tuple(fs.getNode("size").mat().flatten().astype(int))
            cam_matrix = fs.getNode("K"+str(i+1)).mat()
            dist_coefs = fs.getNode("D"+str(i+1)).mat()
            cam = CameraIntrinsics(size, cam_matrix, dist_coefs)
            # Rectification parameters
            R = fs.getNode("R"+str(i+1)).mat()
            P = fs.getNode("P"+str(i+1)).mat()
            cam_rect[i] = CameraRectify(cam, R, P)
        R = fs.getNode("R").mat()
        T = fs.getNode("T").mat()
        # T = np.array([fs.getNode("T").at(j).real() for j in range(fs.getNode("T").size())]) # Read using the same format as in the C++ version
        E = fs.getNode("E").mat()
        F = fs.getNode("F").mat()
        Q = fs.getNode("Q").mat()
        fs.release()
        return StereoCalib(cam_rect, R, T, E, F, Q)


def main():
    parser = argparse.ArgumentParser(description="This program computes the intrinsic calibration of a camera "
                                     "(camera_matrix+distortion) from a sequence of images that observe a checkerboard")
    parser.add_argument("-L", "--img_dir_left", type=str, required=True, help="Directory containing the left-cam images")
    parser.add_argument("-R", "--img_dir_right", type=str, required=True, help="Directory containing the right-cam images")
    parser.add_argument("-mto", "--max_ts_offset", type=int, default=0, help="Maximum stereo offset among left / right images")
    parser.add_argument("-c", "--cols", type=int, required=True, help="Checkerboard width: number of interior columns")
    parser.add_argument("-r", "--rows", type=int, required=True, help="Checkerboard height: number of interior rows")
    parser.add_argument("-s", "--square_size", type=float, required=True, help="Checkerboard square's width (meters)")
    parser.add_argument("-p", "--pyr", type=int, default=0,
                        help="optional parameter to set the number of pyramidal levels to use")
    parser.add_argument("-C", "--confirm", dest='confirm', action='store_true', required=False,
                        help="optional parameter to confirm each detection manually")
    parser.add_argument("-d", "--display", dest='display', action='store_true', required=False,
                        help="optional parameter to display the detections in each images")
    parser.add_argument("-D", "--display_results", dest='display_results', action='store_true', required=False,
                        help="optional parameter to display the useful detections in each image pair")
    parser.add_argument("-o", "--output", type=str, default='', help="Output calibration file")
    args = parser.parse_args()
    print('args', args)

    calib = StereoCalibrator(args.cols, args.rows, args.square_size)
    success, rms = calib.calibrate((args.img_dir_left, args.img_dir_right), args.pyr, args.display, confirm=args.confirm, max_ts_offset=args.max_ts_offset)
    if success:
        if args.output:
            calib.save(args.output, rms)
            print('Calibration saved to:', args.output)

        if args.display_results:
            calib.show_rectified((args.img_dir_left, args.img_dir_right))


if __name__ == "__main__":  # Main program
    main()
