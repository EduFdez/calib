#!/usr/bin/python3

# Copyright (c) 2018 Author: Eduardo Fernandez-Moral
# The CALIB project is released under the 3-clause BSD License
# The License can be consulted in https://gitlab.com/EduFdez/calib/blob/master/LICENSE

from calib_stereo import StereoCalibrator
import os
import glob
import numpy as np
import cv2 as cv
# from matplotlib import pyplot as plt
import argparse
from open3d import *


class StereoDisparity:

    def __init__(self, calib_file=None):
        if calib_file:
            self.calib = StereoCalibrator.from_file(calib_file)
            self.baseline = -self.calib.stereo_calib.T[0, 0]

    @staticmethod
    def compute_disparity(pair_rect, display=False):
        gray_rect = [[]]*2
        for i in range(2):
            if len(pair_rect[i].shape) > 2 and pair_rect[i].shape[2] > 1:
                gray_rect[i] = cv.cvtColor(pair_rect[i], cv.COLOR_BGR2GRAY)
            else:
                gray_rect[i] = pair_rect[i]  # .copy()

        stereo = cv.StereoBM_create(numDisparities=0, blockSize=21)
        # stereo = cv.StereoSGBM_create(minDisparity=0, numDisparities=16, blockSize=21)
        disparity = stereo.compute(gray_rect[0], gray_rect[1])
        if display:
            # Normalize the image for representation
            min_disp = disparity.min()
            max_disp = disparity.max()
            disparity_display = np.uint8(255 * (disparity - min_disp) / (max_disp - min_disp))
            # plt.imshow(np.hstack((gray_rect[0], disparity_display)))#, 'disparity')
            # plt.show()
            cv.imshow('disparity', np.hstack([gray_rect[0], disparity_display]))
            cv.waitKey(0)

        return disparity

    def compute_depth(self, pair_rect, save_depth_img=False):
        disparity = self.compute_disparity(pair_rect)
        depth = self.baseline * self.calib.stereo_calib.cam_rect[0].cam.K[0, 0] / disparity
        return depth

    def get_point_cloud(self, pair_rect, save_ply_file=None):
        disparity = self.compute_disparity(pair_rect)
        xyz = cv.reprojectImageTo3D(disparity, self.calib.stereo_calib.Q)
        colors = cv.cvtColor(pair_rect[0], cv.COLOR_BGR2RGB)
        # if save_ply_file:
        # mask = disparity > disparity.min()
        # out_points = xyz[mask]
        # out_colors = colors[mask]
        # write_ply(save_ply_file, out_points, out_colors)
        # print('%s saved' % 'out.ply')

        # Pass xyz to Open3D.PointCloud and visualize
        pcd = PointCloud()
        pcd.points = Vector3dVector(xyz)
        write_point_cloud(save_ply_file, pcd)

        # Load saved point cloud and visualize it
        pcd_load = read_point_cloud(save_ply_file)
        draw_geometries([pcd_load])

        # convert Open3D.PointCloud to numpy array
        xyz_load = np.asarray(pcd_load.points)
        print('xyz_load')
        print(xyz_load)

        # # save z_norm as an image (change [0,1] range to [0,255] range with uint8 type)
        # img = Image((z_norm*255).astype(np.uint8))
        # write_image(save_depth_img, img)
        # draw_geometries([img])


def read_img_pair(file_pair):
    img_pair = [None]*2
    for i in range(2):
        img_pair[i] = cv.imread(file_pair[i])
    return img_pair


def main():
    parser = argparse.ArgumentParser(description="Compute the disparity/depth from a stereo image pair and display it")
    parser.add_argument("-c", "--calibration", type=str, required=True, help="Calibration file")
    parser.add_argument("-l", "--img_left", type=str, required=True, help="left image")
    parser.add_argument("-r", "--img_right", type=str, required=True, help="right image")
    parser.add_argument("-o", "--output", type=str, required=False, help="Output disparity image")
    parser.add_argument("-d", "--display", dest='display', action='store_true', required=False,
                        help="optional parameter to display the disparity and depth images")
    args = parser.parse_args()
    print('args', args)

    # Get a list with the pairs of stereo images
    pairs = []
    if os.path.isfile(args.img_left) and os.path.isfile(args.img_right):
        pairs = [[args.img_left, args.img_right]]
    elif os.path.isdir(args.img_left) and os.path.isdir(args.img_right):
        left_imgs = {os.path.basename(f):f for f in glob.glob(os.path.join(args.img_left, '*.jpg'))}
        right_imgs = {os.path.basename(f):f for f in glob.glob(os.path.join(args.img_right, '*.jpg'))}
        for i, l in left_imgs.items():
            if i in right_imgs:
                pairs.append([l, right_imgs[i]])
    else:
        raise RuntimeError('The left/right inputs must be either images or directories')

    if args.output:
        # disp_dir = os.path.dirname(args.img_left)
        disp_dir = os.path.join(args.output, 'disp')
        if not os.path.isdir(disp_dir):
            os.makedirs(disp_dir)
        cloud_dir = os.path.join(args.output, 'cloud')

    stereo = StereoDisparity(args.calibration)
    for pair in pairs:
        img_pair = read_img_pair(pair)
        img_rect = stereo.calib.rectify(img_pair)
        if args.display:
            StereoCalibrator.display_epi_lines(img_rect)
        disp = stereo.compute_disparity(img_rect, args.display)
        disp[disp < 0] = 0
        disp_display = np.uint8(255 * disp / disp.max())
        # disp_display = np.uint8(255 * (disp - disp.min()) / (disp.max() - disp.min()))
        # point_cloud = stereo.get_point_cloud(img_rect, args.display)
        if args.output:
            cv.imwrite(os.path.join(disp_dir, os.path.basename(pair[0]))[:-4]+'.png', disp_display)


if __name__ == "__main__":  # Main program
    main()
